window.App.controller('TestCtrl', function ($scope) {
    $scope.suites = window.App.suites;

    $scope.expandConfig = function expandConfig(e) {
        var el = e.srcElement || e.target;
        $(el.parentNode.parentNode).toggleClass('expanded');
    };

    $scope.setActiveSuite = function setActiveSuite(name) {
        $('#suites li').removeClass('selected');
        $('.suite').hide();

        $('#suites li[data-name="' + name + '"]').addClass('selected');
        $('.suite[data-name="' + name + '"]').show();
    };

    $(document).ready(function() {
        $scope.setActiveSuite($scope.suites[0].name);
    });
});

window.App.controller('LogCtrl', function ($scope) { });

