## Precall Test Application ##

[This application](https://bitbucket.org/pallab-gain/precall-test) provides a set of tests to understand internet connection
better before joining a WebRTC call. When joining a call based on webRTC, the user does not know
beforehand how good or bad the call quality will be. For example, if their internet connection is really bad and if the
user knows it before the call, they might opt to join the call without any video, and just enabling audio. This pre-call
test include investigating the capabilities of the internet connection, hardware support details, network health, and other webRTC related
matrices.

## API Documentation ##

[https://substantial-cannon.surge.sh/](https://substantial-cannon.surge.sh/)

## Supported Browser ##
- [x] Google Chrome
- [x] Firefox
- [ ] Opera
- [ ] IE
- [ ] Edge

## Supported Test ##
- Hardware
    - Audio
        - Get list of supported audio hardware
    - Video
        - Get list of supported video hardware
    - Video Resolution
        - Check supported WxH resolution

- Network and Connectivity
    - UDP/TCP/TLS
        - Test if UDP,TCP,TLS connection can be established with relay server.
    - IPv6
        - Test if IPv6 candidates can be gathered
    - Relay
        - Test if connections can be established between peers through Relay server
    - Reflexive
        - Test if connections can be established between peers through NAT
    - Host
        - Test if connections can be established between peers with sample IP address

- Bandwidth
    - Data Throughput
        - Check data throughput of your client by establishing a loop-back call
    - Audio QoS
        - Check audio only call quality your client by establishing a loop-back call
    - AV QoS
        - Check AV call quality your client by establishing a loop-back call

## How does the application works ##
1. Hardware
    1. [Audio](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-50)
        1. Call  `navigator.mediaDevices.enumerateDevices()` to gather list of devices, and return all device that matches `{'kind': 'audioinput'}`
    2. [Video](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-73)
        1. Call  `navigator.mediaDevices.enumerateDevices()` to gather list of devices, and return all device that matches `{'kind': 'videoinput'}`
    3. [Video Resolution](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-95)
        1. Run a test against [pre defined list of resolutions](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-6), and check if current client
        support them using [navigator.mediaDevices.getUserMedia(constraints)](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-110) method.

2. Network and Connectivity
    1. [UDP/TCP/TLS/IPv6](/src/static/library/precalltest/networktest/networkcheck.js)
        1. Try to create a peer connection using relay only server(s), and analyze the gathered ice candidates.
        2. Verify supported connection types using [utils.js](/src/static/library/precalltest/lib/utils.js?#utils.js-45).
    2. [Relay/Reflexive/Host](/src/static/library/precalltest/connectivitytest/connectivitytest.js)
        1. Create a [Datachannel only](/src/static/library/precalltest/connectivitytest/webrtconn.js) loop-back peer connection using specific type.
            1. Relay only
            2. Reflexive
            3. Host only
        2. Check if connections can be established between peers, and data can be exchanges.

3. Bandwidth
    1. Data Throughput
        1. Try to create a [Datachannel only](/src/static/library/precalltest/throughputtest/datathroughtputtest.js) loop-back connection using relay-only connection type.
        2. Exchange data between two peers for certain period of time, and calculate data transmission rate, packet loss
        percentage to get data throughput matrices.
    2. Audio QoS
        1. Try to create a [audio only](/src/static/library/precalltest/throughputtest/audioqos.js) loop-back connection using relay-only connection type.
        2. Run the test for certain period of time, and calculate audio bandwidth, RTT, packet loss of the connection to gather audio only
        quality webRTC call quality matrices.
    3. Audio-Video QoS
        1. Try to create a [Audio+Video](/src/static/library/precalltest/throughputtest/videoqos.js) loop-back connection using relay-only connection type.
        2. Run the test for certain period of time, and calculate AV bandwidth, RTT, packet loss of the connection to gather audio only
        quality webRTC call quality matrices.

## Quality control threshold, and stat ##
- For Audio-Video QoS test; the application used predefined fixed resolution 640x480x30.
- For Data-throughput test we tried to send fixed size buffer ( 1024 bytes ) in each iteration, and try to make sure
  the data send queue is not empty.
- For video resolution support test, we used a fixed set up pre-defined resolutions.
- For all test we used fixed period of time interval ( 30 seconds )
- For relay server we used [callstats.io](https://www.callstats.io/) provided relay server.

- For audio only QoS test you may use [mentioned matrices](https://github.com/opentok/opentok-network-test/blob/master/README.md#audio-only-streams)

    | Quality    | Audio kbps | Packet loss |
    | ---------- | ---------- | ----------- |
    | Excellent  | > 30       | < 0.5%      |
    | Acceptable | > 25       | < 5%        |

- For Audio-video QoS test you may use [mentioned matrices](https://github.com/opentok/opentok-network-test/blob/master/README.md#audio-video-streams)

    | Quality    | Video resolution @ fps | Video kbps  | Packet loss |
    | ---------- | ---------------------- | ----------- | ----------- |
    | Excellent  | 1280x720 @ 30          | > 1000      | < 0.5%      |
    | Excellent  | 640x480 @ 30           | > 600       | < 0.5%      |
    | Excellent  | 352x288 @ 30           | > 300       | < 0.5%      |
    | Excellent  | 320x240 @ 30           | > 300       | < 0.5%      |
    | Acceptable | 1280x720 @ 30          | > 350       | < 3%        |
    | Acceptable | 640x480 @ 30           | > 250       | < 3%        |
    | Acceptable | 352x288 @ 30           | > 150       | < 3%        |
    | Acceptable | 320x240 @ 30           | > 150       | < 3%        |

- For RTT calculation you may use [mentioned matrices](https://www.callstats.io/2015/07/06/basics-webrtc-getstats-api/)

    | MOS Value  | Quality    | RTT             |
    | ---------- | ---------- | ----------------|
    | Bad        | Very poor  | >= 1e3          |
    | Poor       | Poor       | >= 800 && 1e3   |
    | Fair       | Moderate   | >= 600 && <800  |
    | Good       | Acceptable | >= 400 && <600  |
    | Excellent  | Excellent  | < 400           |

## [How to use the Demo](https://precall-test.appspot.com) ##
1. Go to [https://precall-test.appspot.com/](https://precall-test.appspot.com/)
2. The tests are divided into three categories. Each categories has separate tests. You can run any test by clicking "Run".![](/demo/home-page.png?raw=true)
3. You can also change the test duration time, and relay/turn servers that you want to use in your test. ![](/demo/turn-test-duration-change.png?raw=true)
4. A sample test result for audio only QoS test,![](/demo/test-result.png?raw=true)


## Development ##
Make sure to install NodeJS, NPM, Go before continuing. The application is running on google appengine. So, you will also need appengine cloud SDK to run
the code locally.

### Download codebase ###
```
git clone https://pallab-gain@bitbucket.org/pallab-gain/precall-test.git
```

### Install developer tools, and frameworks ###
```
cd precall-test
git checkout appengine-compatible
npm install
```

### Run application locally ###
```
/path/to/go_appengine/goapp serve src/app.yaml
```

Check website : [http://localhost:8080](http://localhost:8080)

## Project tree ##
The codebase is divided into two part.

1. View
    1. Use pre-call test SDK to conduct tests, and show reports, and handle UI components.
    2. ![](/demo/view-tree.png?raw=true)

2. SDK
    1. Codebase for SDK. Compute,conduct, and gather pre-call test reports, and expose
    interface/methods to access the results.
    2. ![](/demo/sdk-tree.png?raw=true)


## Implementation Notes ##
SDK, and implementation details are included in each sub-directory readme.

1. [Todo] Put links here

## Credits ##
1. [https://github.com/opentok/opentok-network-test](https://github.com/opentok/opentok-network-test)
2. [https://github.com/webrtc/testrtc](https://github.com/webrtc/testrtc)
3. [https://www.callstats.io/](https://www.callstats.io/)
4. [https://github.com/muaz-khan/getStats](https://github.com/muaz-khan/getStats)

## Contact ##
Developed by [@pallab-gain](https://bitbucket.org/pallab-gain)














