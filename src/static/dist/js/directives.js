window.App.directive('configOption', function($compile) {
    var textboxTemplate = `
    <label>{{content.label}}</label>
    <input type="textbox" class="option" data-id="{{content.id}}" placeholder="{{content.placeholder}}"
    value="{{content.value}}"/>
  <br/>`;

    var selectTemplate = `
    <label>{{content.label}}</label>
    <select data-id="{{content.id}}" class="option">
      <option ng-repeat="option in content.options" value="{{option.value}}">{{option.name}}</option>
    </select>
  <br/>`;


    var getTemplate = function(optionType) {
        switch (optionType) {
            case 'textbox': return textboxTemplate;
            case 'select': return selectTemplate;
        }
    };

    return {
        restrict: 'E',
        scope: { content:'=' },
        link: function(scope, element, attrs) {
            element.html(getTemplate(scope.content.type)).show();
            $compile(element.contents())(scope);
        }
    };
});

window.App.directive('supported', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if (scope.test.isSupported && !scope.test.isSupported()) {
                element.addClass('disabled');
                $('button', element).prop('disabled', true);
            }
        }
    };
});

window.App.directive('runOnClick', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var test = scope.test;

            element.on('click', function(e) {
                e.preventDefault();

                if (App.isRunning) {
                    return App.log(true, new Error('Test not starting. Tests already running'));
                }

                App.isRunning = true;
                $('body').addClass('running');
                $('.test-header > button').prop('disabled', true);
                App.log(false, 'Test "' + test.name + '" started...');

                var options = { };
                $('.option', element.parentNode).each(function(i, el) {
                    var data_id = $(el).attr('data-id');
                    if( data_id.indexOf(test.name)>-1 ) {
                        options[ data_id.replace(test.name,"") ] = $(el).val();
                    }
                });

                function progress(percent) {
                    $('#progress-bar').width(percent+'%');
                }

                var reporters = {
                    log: App.log.bind(null, false),
                    progress: progress
                };

                try {
                    test.run(window, options, reporters, function done(finalMessage) {
                        var args = [true].concat(Array.from(arguments));
                        if (finalMessage) { App.log.apply(null, args); }
                        App.isRunning = false;
                        $('body').removeClass('running');
                        $('.test-header > button').prop('disabled', false);
                        progress(0);
                        App.log(false, 'Test "' + test.name + '" ended');
                    });
                } catch (e) {
                    App.isRunning = false;
                    $('body').removeClass('running');
                    $('.test-header > button').prop('disabled', false);
                    progress(0);
                    App.log(true, new Error('Could not run test "' + test.name + ": " + e));
                }
            });
        }
    };
});

