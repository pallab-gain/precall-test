(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.WebRTCPreCallTest = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

/**
 * @class Gather current browser detail, it's version, and whether the browser support webRTC.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Browser = function () {
    /** @constructs */
    function Browser() {
        _classCallCheck(this, Browser);

        var self = this;
    }

    /**
     * Get the current browser details
     *
     * @returns {Object} represents browserType, version, and supportWebRTC details of the browser
     */


    _createClass(Browser, [{
        key: 'getDetails',
        value: function getDetails() {
            var retval = adapter.browserDetails;
            return {
                browserType: retval.browser,
                version: retval.version,
                supportWebRTC: retval.version == null ? false : true
            };
        }
    }]);

    return Browser;
}();

exports.Browser = Browser;

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Connectivity = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require("../lib/utils");

var _webrtconn = require("../connectivitytest/webrtconn");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Verify connections.
 * Establish a loopback connection using
 *  Relay,
 *  Reflexive,
 *  Host only
 *  And check whether connection can be established, and data can be exchange between peer.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

var Connectivity = function () {
    /** @constructs
     * @param {Object} configs - Test Duration. Number of time in second the test will run
     */
    function Connectivity(configs) {
        _classCallCheck(this, Connectivity);

        var self = this;
        self.configs = _.clone(configs, true);
        self.util = new _utils.Utils();
        self.testDuration = parseFloat(configs['test-timeout']);
        self.timeout_id = undefined;
    }

    /**
     * Check connectivity detail
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @returns {Promise} Promise object that contains Relay, Reflexive, Host only connection support details.
     */


    _createClass(Connectivity, [{
        key: "checkConnectivity",
        value: function checkConnectivity(progress, log) {
            progress = progress || function () {};
            log = log || function () {};
            var self = this;
            progress(0);
            var data = {
                'Relay': { 'support': false, 'data': undefined },
                'Reflexive': { 'support': false, 'data': undefined },
                'Host': { 'support': false, 'data': undefined }
            };

            return new Promise(function (resolve, reject) {
                function check(process, isGood) {
                    return new Promise(function (resolve, reject) {
                        process.testConnectivity(isGood).then(function (success) {
                            resolve(success);
                        }).catch(function (err) {
                            resolve(false);
                        });
                    });
                }

                log("Info : Checking relayed connectivity...");
                progress(5);
                var relay = new _webrtconn.WebRTCCon(self.configs);
                check(relay, self.util.isRelay).then(function (success) {
                    progress(20);
                    data.Relay.support = success;
                    var reflex = new _webrtconn.WebRTCCon(self.configs);
                    log("Info : Checking reflexive connectivity...");
                    check(reflex, self.util.isReflexive).then(function (success) {
                        progress(40);
                        var host = new _webrtconn.WebRTCCon(self.configs);
                        data.Reflexive.support = success;
                        log("Info : Checking host connectivity...");
                        check(new _webrtconn.WebRTCCon(self.configs), self.util.isHost).then(function (success) {
                            progress(80);
                            data.Host.support = success;
                            resolve(data);
                        });
                    });
                });
            });
        }
    }]);

    return Connectivity;
}();

exports.Connectivity = Connectivity;

},{"../connectivitytest/webrtconn":3,"../lib/utils":7}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.WebRTCCon = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require('../lib/utils');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Core WebRTC connection class.
 * Establish a p2p connection between two peer with provided connection type.( loop-back connection )
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

var WebRTCCon = function () {
    /** @constructs
     * @param {Object} configs - Test Duration, Turn details
     *
     */
    function WebRTCCon(configs) {
        _classCallCheck(this, WebRTCCon);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;
        self.dc1 = undefined;
        self.canExchangeData = false;
        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.testDuration = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
        self.timeout_id = undefined;
        self.callback = {
            onSuccess: undefined
        };
    }
    /**
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(WebRTCCon, [{
        key: '_getRemoteTurn',
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }
        /**
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: '_createPC',
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }

        /**
         * @param {Object} peerConnection - PeerConnection object
         * @returns {Object} DataChannel with associated peer connection
         */

    }, {
        key: '_createDC',
        value: function _createDC(peerConnection) {
            var self = this;
            return peerConnection.createDataChannel(null);
        }

        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: '_closePC',
        value: function _closePC() {
            var self = this;
            self.pc1.close();
            self.pc2.close();
            self.pc1 = null;
            self.pc2 = null;
        }

        /**
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: '_registerIceEvenets',
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            isGood = isGood || function () {};
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
        }
        /**
         * @param {Object} dc - DataChannel the loopback session
         * @param {Object} pc - Associated peer connection with given data channel
         */

    }, {
        key: '_registerDCEvenets',
        value: function _registerDCEvenets(dc, pc) {
            var self = this;

            var data = 'hello from peer remote peer';
            dc.addEventListener('open', function (e) {
                dc.send(data);
            });

            dc.addEventListener('message', function (e) {
                if (e.data !== data) {
                    console.error('Invalid data transmitted.');
                } else {
                    self.canExchangeData = true;
                    console.log('can exchange data ', true);
                    self.callback.onSuccess(self.canExchangeData);
                }
            });

            pc.addEventListener('datachannel', function (e) {
                var remoteDC = e.channel;
                remoteDC.addEventListener('message', function (evt) {
                    if (evt.data !== data) {
                        console.error('Invalid data transmitted.');
                    } else {
                        remoteDC.send(data);
                    }
                });
            });
        }

        /**
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents boolean true or false
         */

    }, {
        key: '_mayBeStartStreaming',
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.canExchangeData = false;
            console.log('test duration ', self.testDuration);
            return new Promise(function (resolve, reject) {
                var timeoutID = setTimeout(function (noop) {
                    console.log('end test from timeout ');
                    resolve(self.canExchangeData);
                }, self.testDuration * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                    self.callback.onSuccess = function (success) {
                        clearTimeout(timeoutID);
                        console.log('end test from callback');
                        resolve(success);
                    };
                }).catch(function (err) {
                    if (typeof timeoutID != 'undefined') {
                        clearTimeout(timeoutID);
                    }
                    reject(err);
                });
            });
        }
        /**
         * @param {Function} isGood - Supported Relay candidate type
         * @returns {Promise} A promise object that represents boolean true or false
         */

    }, {
        key: 'testConnectivity',
        value: function testConnectivity(isGood) {
            var self = this;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, isGood);
            self._registerIceEvenets(self.pc2, self.pc1, isGood);

            self.dc1 = self._createDC(self.pc1);
            self._registerDCEvenets(self.dc1, self.pc2);

            return new Promise(function (resolve, reject) {
                self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                    self._closePC();
                    resolve(success);
                }).catch(function (err) {
                    self._closePC();
                    reject(err);
                });
            });
        }
    }]);

    return WebRTCCon;
}();

exports.WebRTCCon = WebRTCCon;

},{"../lib/utils":7}],4:[function(require,module,exports){
'use strict';

/**
 * @class Provides available audio, video hardware details. Also provides list of supported video resolutions.
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Hardware = function () {
    /** @constructs */
    function Hardware() {
        _classCallCheck(this, Hardware);

        var self = this;
        self.resolutions = [{ res: [160, 120], support: false }, { res: [320, 180], support: false }, { res: [640, 360], support: false }, { res: [768, 576], support: false }, { res: [1024, 576], support: false }, { res: [1280, 720], support: false }, { res: [1280, 768], support: false }, { res: [1280, 800], support: false }, { res: [1920, 1080], support: false }, { res: [1920, 1200], support: false }, { res: [3840, 2160], support: false }, { res: [4096, 2160], support: false }];
    }
    /**
     * Return available video input devices.
     * @returns {Object} List of available audio, and video devices.
     */


    _createClass(Hardware, [{
        key: '_deviceList',
        value: function _deviceList() {
            return navigator.mediaDevices.enumerateDevices();
        }
        /**
         * @returns {Object} List of available audio input devices.
         */

    }, {
        key: '_audioDeviceList',
        value: function _audioDeviceList() {
            var self = this;
            return new Promise(function (resolve, reject) {
                self._deviceList().then(function (deviceList) {
                    var dlist = _.filter(deviceList, { 'kind': 'audioinput' });
                    resolve(dlist);
                }).catch(function (err) {
                    reject(err);
                });
            });
        }
        /**
         * Return available video input devices.
         * @returns {Object} List of available video input devices.
         */

    }, {
        key: '_videoDeviceList',
        value: function _videoDeviceList() {
            var self = this;
            return new Promise(function (resolve, reject) {
                self._deviceList().then(function (deviceList) {
                    var dlist = _.filter(deviceList, { 'kind': 'videoinput' });
                    resolve(dlist);
                }).catch(function (err) {
                    reject(err);
                });
            });
        }
        /**
         * Returns list of available audio input devices
         * @returns {Promise} Promise object represents list of available audio input devices
         */

    }, {
        key: 'audioSupport',
        value: function audioSupport() {
            var self = this;
            var promise = self._audioDeviceList();
            return new Promise(function (resolve, reject) {
                var media = {
                    deviceSupport: false,
                    deviceList: []
                };
                promise.then(function (success) {
                    media.deviceList = success;
                    media.deviceSupport = _.size(media.deviceList) > 0 ? true : false;
                    if (media.deviceSupport == false) {
                        resolve(media);
                    } else {
                        resolve(media);
                    }
                }).catch(function (err) {
                    reject(media);
                });
            });
        }
        /**
         * Returns list of available video input devices
         * @returns {Promise} Promise object represents list of available video input devices
         */

    }, {
        key: 'videoSupport',
        value: function videoSupport() {
            var self = this;
            var promise = self._videoDeviceList();
            return new Promise(function (resolve, reject) {
                var media = {
                    deviceSupport: false,
                    deviceList: []
                };
                promise.then(function (success) {
                    media.deviceList = success;
                    media.deviceSupport = _.size(media.deviceList) > 0 ? true : false;
                    if (media.deviceSupport == false) {
                        resolve(media);
                    } else {
                        resolve(media);
                    }
                }).catch(function (err) {
                    reject(media);
                });
            });
        }

        /**
         * Returns list of supported video resolution.
         * @param {function} progress - binder to show progress in UI
         * @returns {Promise} Promise object represents list of supported video resolution.
         */

    }, {
        key: 'resolutionTest',
        value: function resolutionTest(progress) {
            progress = progress || function () {};
            var self = this;
            var totalLength = self.resolutions.length;
            var totalTested = 0;
            progress(0);
            return new Promise(function (resolve, reject) {
                for (var indx in self.resolutions) {
                    (function (curIndx) {
                        var constraints = {
                            audio: false,
                            video: {
                                width: { exact: self.resolutions[curIndx].res[0] },
                                height: { exact: self.resolutions[curIndx].res[1] }
                            }
                        };
                        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
                            self.resolutions[curIndx].support = true;
                            stream.getTracks().forEach(function (track) {
                                track.stop();
                            });
                            totalTested += 1;
                            progress(Math.round(100. * totalLength / totalLength));
                            if (totalTested >= totalLength) {
                                resolve(self.resolutions);
                            }
                        }).catch(function (err) {
                            self.resolutions[curIndx].support = false;
                            totalTested += 1;
                            progress(Math.round(100. * totalLength / totalLength));
                            if (totalTested >= totalLength) {
                                resolve(self.resolutions);
                            }
                        });
                    })(indx);
                }
            });
        }
        /**
         * Returns list of supported devices
         * @param {bool} isAudio - Device type is audio or video
         * @returns {Promise} Promise object represents list of supported video resolution.
         */

    }, {
        key: 'supportDetails',
        value: function supportDetails(isAudio) {
            var self = this;
            var data = {
                'data': undefined, 'err': undefined
            };
            return new Promise(function (resolve, reject) {
                var promise = isAudio ? self.audioSupport() : self.videoSupport();

                promise.then(function (success) {
                    data.data = success;
                    resolve(data);
                }).catch(function (err) {
                    data.err = err;
                    resolve(data);
                });
            });
        }
        /**
         * Returns list of supported video resolution
         * @param {function} progress binder to show progress in UI
         * @returns {Promise} Promise object represents list of supported video resolution.
         */

    }, {
        key: 'resolutionSupport',
        value: function resolutionSupport(progress) {
            var self = this;
            return new Promise(function (resolve, reject) {
                self.resolutionTest(progress).then(function (success) {
                    resolve(success);
                }).catch(function (err) {
                    resolve(err);
                });
            });
        }
    }]);

    return Hardware;
}();

exports.Hardware = Hardware;

},{}],5:[function(require,module,exports){
'use strict';

/**
 * @class HTTPClient wrapper.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HTTPClient = function () {
    /** @constructs */
    function HTTPClient() {
        _classCallCheck(this, HTTPClient);

        var self = this;
    }

    /**
     * Provide a 12 digit random string
     * @returns {string} 12 digit long random string
     */


    _createClass(HTTPClient, [{
        key: "_get_transaction_id",
        value: function _get_transaction_id() {
            return Math.random().toString(14).substr(2, 12);
        }
    }, {
        key: "do_post",


        /**
         * POST call wrapper.
         * @param {number} time - Current time.
         * @param {number} datapoint - current value
         * @returns {Promise} Promise object that contains jquery response object.
         */
        value: function do_post(url, timeOut, data) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": 'POST',
                    "timeout": timeOut,
                    "headers": {
                        "content-type": "application/json"
                    },
                    "processData": false,
                    "data": data,
                    "success": function success(response) {
                        resolve(response);
                    },
                    "error": function error(xhr, status, _error) {
                        console.log(xhr, status, _error);
                        reject(_error);
                    }
                });
            });
        }

        /**
         * Do a simple GET request to check whether network is available.
         * @param {string} url - URL of network request
         * @param {number} timeout - Timeout of current ajax request
         * @returns {Promise} Promise object that contains jquery response object.
         */

    }, {
        key: "network_test",
        value: function network_test(url, timeOut) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET",
                    "timeout": timeOut,
                    "success": function success(response) {
                        resolve(response);
                    },
                    "error": function error(xhr, status, _error2) {
                        reject(xhr);
                    }
                });
            });
        }
    }]);

    return HTTPClient;
}();

exports.HTTPClient = HTTPClient;

},{}],6:[function(require,module,exports){
'use strict';

/**
 * @class Stats class that collect rtt, bwe values and
 * provides aggregated value
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StatisticsAggregate = function () {
    /** @constructs
     * @param {number} rampUpThreshold - Default ramp up threshold value for the given test.
     */
    function StatisticsAggregate(rampUpThreshold) {
        _classCallCheck(this, StatisticsAggregate);

        var self = this;
        self.startTime_ = 0;
        self.sum_ = 0;
        self.count_ = 0;
        self.max_ = 0;
        self.rampUpThreshold_ = rampUpThreshold;
        self.rampUpTime_ = Infinity;
    }
    /**
     * Check connectivity detail
     * @param {number} time - Current time.
     * @param {number} datapoint - current value
     */


    _createClass(StatisticsAggregate, [{
        key: 'add',
        value: function add(time, datapoint) {
            var self = this;
            if (self.startTime_ === 0) {
                self.startTime_ = time;
            }
            this.sum_ += datapoint;
            self.max_ = Math.max(self.max_, datapoint);
            if (self.rampUpTime_ === Infinity && datapoint > self.rampUpThreshold_) {
                self.rampUpTime_ = time;
            }
            self.count_++;
        }
        /**
         * Get average value
         * @returns {number} Average value
         */

    }, {
        key: 'getAverage',
        value: function getAverage() {
            var self = this;
            if (self.count_ === 0) {
                return 0;
            }
            return Math.round(self.sum_ / self.count_);
        }
        /**
         * Get Maximum value
         * @returns {number} Maximum value
         */

    }, {
        key: 'getMax',
        value: function getMax() {
            var self = this;
            return self.max_;
        }
        /**
         * Get raqmp up time value
         * @returns {number} RampUp time.
         */

    }, {
        key: 'getRampUpTime',
        value: function getRampUpTime() {
            var self = this;
            return Math.round(self.rampUpTime_ - self.startTime_);
        }
    }]);

    return StatisticsAggregate;
}();

exports.StatisticsAggregate = StatisticsAggregate;

},{}],7:[function(require,module,exports){
'use strict';

/**
 * @class Utility class specific to this pre-call test.
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Utils = function () {
    /** @constructs */
    function Utils() {
        _classCallCheck(this, Utils);

        var self = this;
    }
    /**
     * Return transport type
     * @param {protocol} protocol - WebRTC protocol type
     * @param {transport} tranport - Transport candidate
     * @returns {string} Transport type. TLS, TCP, or UDP
     */


    _createClass(Utils, [{
        key: "_transportType",
        value: function _transportType(protocol, tranport) {
            var self = this;
            var b = "NONE",
                c = tranport >> 24;
            if ("rtp" === protocol && c >= 0 && c <= 2) {
                switch (c) {
                    case 0:
                        b = "TLS";
                        break;
                    case 1:
                        b = "TCP";
                        break;
                    case 2:
                        b = "UDP";
                }
            }
            return b;
        }
        /**
         * Return object representation of parsed ice candidate
         * @param {string} text - WebRTC ICE candidate
         * @returns {Object} JSON object representation of ICE candidate.
         */

    }, {
        key: "parseCandidate",
        value: function parseCandidate(text) {
            var self = this;

            var candidateStr = 'candidate:';
            var pos = text.indexOf(candidateStr) + candidateStr.length;
            var fields = text.substr(pos).split(' ');

            return {
                'protocol': "1" === fields[1] ? "rtp" : "rtcp",
                'transport': fields[2],
                'typeTransport': self._transportType("1" === fields[1] ? "rtp" : "rtcp", fields[3]),
                'ipv6': -1 !== fields[4].indexOf(":"),
                'ipAddress': fields[4],
                'port': fields[5],
                'type': fields[7]
            };
        }
    }, {
        key: "isUDP",

        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support UDP
         */
        value: function isUDP(e) {
            return e.typeTransport === 'UDP';
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support TCP
         */

    }, {
        key: "isTCP",
        value: function isTCP(e) {
            return e.typeTransport === 'TCP';
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support TLS
         */

    }, {
        key: "isTLS",
        value: function isTLS(e) {
            return e.typeTransport === 'TLS';
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support IPv6
         */

    }, {
        key: "isIPv6",
        value: function isIPv6(e) {
            return e.ipv6;
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support Relay
         */

    }, {
        key: "isRelay",
        value: function isRelay(e) {
            return e.type === "relay";
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support non Host
         */

    }, {
        key: "isNotHostCandidate",
        value: function isNotHostCandidate(e) {
            return e.type === "host";
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support srflx
         */

    }, {
        key: "isReflexive",
        value: function isReflexive(e) {
            return e.type === "srflx";
        }
        /**
         * Return transport type
         * @param {string} e - WebRTC ICE candidate
         * @returns {bool} is support Host
         */

    }, {
        key: "isHost",
        value: function isHost(e) {
            return e.type === "host";
        }
        /**
         * Return transport type
         * @param {array} tracks - WebRTC media track
         * @returns {string} label of track
         */

    }, {
        key: "getDeviceName_",
        value: function getDeviceName_(tracks) {
            if (tracks.length === 0) {
                return null;
            }
            return tracks[0].label;
        }
        /**
         * Return local media stream. May need to grant permission
         * @param {Object} constraints - getUser media constrain
         * @param {onSuccess} onSuccess - The callback that handles getUserMedia success, and return local stream as a callback parameter
         * @param {onFail} onFail - The callback that handles getUserMedia failuer
         */

    }, {
        key: "doGetUserMedia",
        value: function doGetUserMedia(constraints, onSuccess, onFail) {
            var self = this;
            try {
                // Call into getUserMedia via the polyfill (adapter.js).
                navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
                    var cam = self.getDeviceName_(stream.getVideoTracks());
                    var mic = self.getDeviceName_(stream.getAudioTracks());
                    onSuccess.apply(this, arguments);
                }).catch(function (error) {
                    if (onFail) {
                        onFail.apply(this, arguments);
                    } else {
                        console.error(error);
                    }
                });
            } catch (e) {
                console.error(error);
            }
        }
        /**
         * Empty function
         */

    }, {
        key: "noop",
        value: function noop() {}
    }]);

    return Utils;
}();

exports.Utils = Utils;

},{}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.AvailabilityCheck = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _httpclient = require('../lib/httpclient/httpclient');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Verify is network is available.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

var AvailabilityCheck = function () {
    /** @constructs
     * @param {Object} configs - Test Duration. Number of time in second the test will run
     */
    function AvailabilityCheck(configs) {
        _classCallCheck(this, AvailabilityCheck);

        var self = this;
        self.testDuration = parseFloat(configs['test-timeout']);
        self.timeout_id = undefined;
    }
    /**
    * @returns {Promise} Promise object that contains whether network is available, no-data, or unavailable.
    */


    _createClass(AvailabilityCheck, [{
        key: 'isNetworkAvailable',
        value: function isNetworkAvailable() {
            var self = this;
            var isOnline = false;
            if (navigator && navigator.onLine) {
                isOnline = navigator.onLine;
            }
            if (typeof self.timeout_id != 'undefined') {
                clearTimeout(self.timeout_id);
                self.timeout_id = undefined;
            }

            return new Promise(function (resolve, reject) {
                self.timeout_id = setTimeout(function () {
                    resolve(false);
                }, self.testDuration * 1000);

                if (isOnline != true) {
                    resolve(true);
                } else {
                    // const generate_204 = 'http://clients3.google.com/generate_204';
                    var generate_204 = 'https://api.github.com/users/octocat/orgs';
                    var httpclick = new _httpclient.HTTPClient();
                    httpclick.network_test(generate_204, 5 * 1000).then(function (success) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }
            });
        }
    }]);

    return AvailabilityCheck;
}();

exports.AvailabilityCheck = AvailabilityCheck;

},{"../lib/httpclient/httpclient":5}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Network = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require('../lib/utils');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Verify connectivity support list.
 * Make a peer connection using relay server, and verify if connection supports
 *  TCP,
 *  TLS,
 *  UDP
 *  IPv6
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

var Network = function () {
    /** @constructs
     * @param {Object} configs - Test Duration, Turn details
     */
    function Network(configs) {
        _classCallCheck(this, Network);

        var self = this;
        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.testDuration = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
        self.timeout_id = undefined;
        self.progress = undefined;
        self.log = undefined;
    }
    /**
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(Network, [{
        key: 'getRemoteTurn',
        value: function getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }
        /**
         * Collect webRTC ICE candidates
         * @param {function} progress - binder to show progress in UI
         * @param {Object} iceConfigs - Turn ICE configuration
         * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
         */

    }, {
        key: 'collectCandidates',
        value: function collectCandidates(progress, iceConfigs) {
            progress = progress || function () {};
            var self = this;
            var config = iceConfigs;
            progress(0);
            var data = {
                'TCP': { 'support': false, 'data': undefined },
                'TLS': { 'support': false, 'data': undefined },
                'UDP': { 'support': false, 'data': undefined },
                'IPv6': { 'support': false, 'data': undefined }
            };

            return new Promise(function (resolve, reject) {
                if (typeof self.timeout_id != 'undefined') {
                    clearTimeout(self.timeout_id);
                    self.timeout_id = undefined;
                }
                self.timeout_id = setTimeout(function () {
                    progress(100);
                    if (typeof pc != 'undefined') {
                        pc.close();
                        pc = null;
                    }
                    resolve(data);
                }, self.testDuration * 1000);

                var pc = undefined;
                try {
                    pc = new RTCPeerConnection(config, null);
                } catch (error) {
                    console.error(error);
                    reject(error);
                }
                pc.addEventListener('icecandidate', function (e) {
                    // Once we've decided, ignore future callbacks.
                    if (e.currentTarget.signalingState === 'closed') {
                        return;
                    }

                    if (e.candidate) {
                        progress(80);
                        var parsed = self.util.parseCandidate(e.candidate.candidate);
                        if (self.util.isTCP(parsed)) {
                            data.TCP = { 'support': true, 'data': parsed };
                        } else if (self.util.isTLS(parsed)) {
                            data.TLS = { 'support': true, 'data': parsed };
                        } else if (self.util.isUDP(parsed)) {
                            data.UDP = { 'support': true, 'data': parsed };
                        } else if (self.util.isIPv6(parsed)) {
                            data.IPv6 = { 'support': true, 'data': parsed };
                        }
                    } else {
                        if (typeof self.timeout_id != 'undefined') {
                            clearTimeout(self.timeout_id);
                            self.timeout_id = undefined;
                        }

                        progress(100);
                        pc.close();
                        pc = null;
                        resolve(data);
                    }
                });
                var offerParam = { offerToReceiveAudio: 1 };
                pc.createOffer(offerParam).then(function (offer) {
                    progress(30);
                    pc.setLocalDescription(offer).then(self.util.noop, self.util.noop);
                }, self.util.noop);
            });
        }
        /**
         * Get network connectivity details
         * @param {function} progress - binder to show progress in UI
         * @param {function} log binder to log in UI
         * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
         */

    }, {
        key: 'getNetworkDetail',
        value: function getNetworkDetail(progress, log) {
            progress = progress || function () {};
            var self = this;
            self.progress = progress;
            self.log = log;
            var candidates = self.getRemoteTurn(true);
            candidates = self.getRemoteTurn(false);
            return self.collectCandidates(progress, candidates);
        }
    }]);

    return Network;
}();

exports.Network = Network;

},{"../lib/utils":7}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ChromeAudioQoS = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require("../lib/utils");

var _stats = require("../lib/stats");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Provides Google Chrome specific Audio only call quality matrices.
 * Try to create a Audio only loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate audio bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
var ChromeAudioQoS = function () {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    function ChromeAudioQoS(configs) {
        _classCallCheck(this, ChromeAudioQoS);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': []

            },
            'stats': undefined
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
    }
    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(ChromeAudioQoS, [{
        key: "_getRemoteTurn",
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }

        /**
         * Create peer connection object
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: "_createPC",
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }

        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: "_closePC",
        value: function _closePC() {
            console.log('closing peer connection');
            var self = this;
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            if (typeof self.pc1 != 'undefined') {
                self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                    track.stop();
                });
                self.pc1.close();
                self.pc1 = undefined;
            }
            if (typeof self.pc2 != 'undefined') {
                self.pc2.close();
                self.pc2 = undefined;
            }
        }
        /**
         * Collect webRTC matrices for audio only loop-back call.
         * @param {Object} result - webRTC stats object
         * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: "_goStats",
        value: function _goStats(result, isLocal, self) {
            if (isLocal == true) {
                self.rtcstat.stats = result;
            }

            function __collect(obj, item) {
                var isSending = item.id.indexOf('_send') !== -1; // sender or receiver
                var sendBandWidth = 0.008 * self.rtcstat.stats.bandwidth.speed;
                if (isSending == true) {
                    if (isLocal == true && typeof self.pc1 != 'undefined' && sendBandWidth > 0) {
                        self.ITR += 1;
                        self.progress(Math.round(100. * self.ITR) / self.UPTO);
                        self.log('Info : current audio transmitting rate ' + sendBandWidth.toFixed(2) + " kbps");
                        obj.bandwidth_ar.push(parseFloat(sendBandWidth.toFixed(2)));
                    }
                    if (typeof item.googRtt != 'undefined') {
                        obj.rtt.add(item.timestamp, parseFloat(item.googRtt));
                        obj.rtt_ar.push(parseFloat(item.googRtt));
                    }
                    if (typeof item.packetsSent != 'undefined') {
                        obj.packetsent = item.packetsSent;
                    }
                } else {
                    if (typeof item.packetsLost != 'undefined') {
                        obj.packetloss = item.packetsLost;
                    }
                }
            }
            if (typeof result.results != 'undefined') {
                result.results.forEach(function (item) {
                    if (item.type === 'ssrc' && item.transportId === 'Channel-audio-1') {
                        var isAudio = item.mediaType === 'audio'; // audio or video
                        __collect(isAudio == true ? self.rtcstat.audio : self.rtcstat.video, item);
                    }
                });
            } else {
                console.log('what -> ', result);
            }
            if (isLocal == true && typeof self.pc1 == 'undefined') {
                result.nomore();
            }
            if (isLocal == false && typeof self.pc2 == 'undefined') {
                result.nomore();
            }
        }
        /**
         * Register ICE events
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: "_registerIceEvenets",
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
            pc2.addEventListener('addstream', function (e) {
                getStats(self.pc1, function (result) {
                    self._goStats(result, true, self);
                }, 1 * 1000);
                getStats(self.pc2, function (result) {
                    self._goStats(result, false, self);
                }, 1 * 1000);
            });
        }
        /**
         * Collect, finalize audio only loopback call stat after completing the full session.
         *
         * @returns {Object} Audio only call quality control matrices
         */

    }, {
        key: "_collect_stat",
        value: function _collect_stat() {
            var self = this;
            function __getstat(data) {
                var avgRTT = data.rtt.getAverage();
                var maxRTT = data.rtt.getMax();
                var packetLossPercentage = 100.0 * data.packetloss / data.packetsent;
                return {
                    avgRTT: avgRTT.toFixed(2),
                    maxRTT: maxRTT.toFixed(2),
                    packetLoss: packetLossPercentage.toFixed(2),
                    rtt_ar: data.rtt_ar,
                    bandwidth_ar: data.bandwidth_ar
                };
            }

            var sendBandWidth = 0.008 * _.get(self.rtcstat, "stats.bandwidth.speed", 0);
            return {
                data: __getstat(self.rtcstat.audio),
                bandWidthInkbps: sendBandWidth.toFixed(2)
            };
        }

        /**
         * Try to create a loopback peer connection using relay server
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents webrtc audio only call quality stats objects
         */

    }, {
        key: "_mayBeStartStreaming",
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.rtcstat = {
                'audio': {
                    'packetsent': -1,
                    'packetloss': -1,
                    'rtt': new _stats.StatisticsAggregate(),
                    'bandwidth_ar': [],
                    'rtt_ar': []
                },
                'stats': undefined
            };
            self.ITR = 0;
            return new Promise(function (resolve, reject) {
                if (typeof self.runtest_id != 'undefined') {
                    clearTimeout(self.runtest_id);
                    self.runtest_id = undefined;
                }

                self.runtest_id = setTimeout(function () {
                    var audioQoS = self._collect_stat.apply(self);
                    resolve(audioQoS);
                }, self.UPTO * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        }
        /**
         * Provides audio only call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
         */

    }, {
        key: "testQoS",
        value: function testQoS(progress, log) {
            progress = progress || function () {};
            log = log || function () {};

            var self = this;
            self.progress = progress;
            self.log = log;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
            self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

            var constraints = {
                audio: true,
                video: false
            };

            return new Promise(function (resolve, reject) {
                //get user media
                self.util.doGetUserMedia(constraints, function (stream) {
                    self.pc1.addStream(stream);
                    self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                        self._closePC();
                        resolve(success);
                    }).catch(function (err) {
                        self._closePC();
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            });
        }
    }]);

    return ChromeAudioQoS;
}();

exports.ChromeAudioQoS = ChromeAudioQoS;

},{"../lib/stats":6,"../lib/utils":7}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ChromeVideoQoS = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require("../lib/utils");

var _stats = require("../lib/stats");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Provides Google Chrome specific Audio Video  call quality matrices.
 * Try to create a Audio+Video loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate AV bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
var ChromeVideoQoS = function () {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    function ChromeVideoQoS(configs) {
        _classCallCheck(this, ChromeVideoQoS);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': []
            },
            'video': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': []
            },
            'stats': undefined
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
    }
    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(ChromeVideoQoS, [{
        key: "_getRemoteTurn",
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }

        /**
         * Create peer connection object
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: "_createPC",
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }

        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: "_closePC",
        value: function _closePC() {
            console.log('closing peer connection');
            var self = this;
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            if (typeof self.pc1 != 'undefined') {
                self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                    track.stop();
                });
                self.pc1.close();
                self.pc1 = undefined;
            }
            if (typeof self.pc2 != 'undefined') {
                self.pc2.close();
                self.pc2 = undefined;
            }
        }
        /**
         * Collect webRTC matrices for audio video loop-back call.
         * @param {Object} result - webRTC stats object
         * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: "_goStats",
        value: function _goStats(result, isLocal, self) {
            if (isLocal == true) {
                self.rtcstat.stats = result;
            }

            function __collect(obj, item, isAudio) {
                var isSending = item.id.indexOf('_send') !== -1; // sender or receiver
                var sendBandWidth = 0.008 * _.get(self.rtcstat, "stats.bandwidth.speed", 0);

                if (isSending == true) {
                    if (isLocal == true && typeof self.pc1 != 'undefined' && sendBandWidth > 0) {
                        self.ITR += 1;
                        self.progress(Math.round(100. * self.ITR) / self.UPTO);
                        if (isAudio == false) {
                            self.log('Info : current transmission rate ' + sendBandWidth.toFixed(2) + " kbps");
                            obj.bandwidth_ar.push(parseFloat(sendBandWidth.toFixed(2)));
                        }
                    }

                    if (typeof item.googRtt != 'undefined') {
                        obj.rtt.add(item.timestamp, parseFloat(item.googRtt));
                        obj.rtt_ar.push(parseFloat(item.googRtt));
                    }
                    if (typeof item.packetsSent != 'undefined') {
                        obj.packetsent = item.packetsSent;
                    }
                } else {
                    if (typeof item.packetsLost != 'undefined') {
                        obj.packetloss = item.packetsLost;
                    }
                }
            }

            result.results.forEach(function (item) {
                if (item.type === 'ssrc' && item.transportId === 'Channel-audio-1') {
                    var isAudio = item.mediaType === 'audio'; // audio or video
                    __collect(isAudio == true ? self.rtcstat.audio : self.rtcstat.video, item, isAudio);
                }
            });
            if (isLocal == true && typeof self.pc1 == 'undefined') {
                result.nomore();
            }
            if (isLocal == false && typeof self.pc2 == 'undefined') {
                result.nomore();
            }
        }
        /**
         * Register ICE events
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: "_registerIceEvenets",
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
            pc2.addEventListener('addstream', function (e) {
                getStats(self.pc1, function (result) {
                    self._goStats(result, true, self);
                }, 1 * 1000);
                getStats(self.pc2, function (result) {
                    self._goStats(result, false, self);
                }, 1 * 1000);
            });
        }
        /**
         * Collect, finalize audio video loopback call stat after completing the full session.
         *
         * @returns {Object} Audio Video call quality control matrices
         */

    }, {
        key: "_collect_stat",
        value: function _collect_stat() {
            var self = this;
            function __getstat(data) {
                var avgRTT = data.rtt.getAverage();
                var maxRTT = data.rtt.getMax();
                var packetLossPercentage = 100.0 * data.packetloss / data.packetsent;

                return {
                    avgRTT: avgRTT.toFixed(2),
                    maxRTT: maxRTT.toFixed(2),
                    packetLoss: packetLossPercentage.toFixed(2),
                    rtt_ar: data.rtt_ar,
                    bandwidth_ar: data.bandwidth_ar
                };
            }
            var sendBandWidth = 0.008 * self.rtcstat.stats.bandwidth.speed;
            var resolution = self.rtcstat.stats.resolutions.send;
            return {
                audio: {
                    data: __getstat(self.rtcstat.audio)
                },
                video: {
                    data: __getstat(self.rtcstat.video)
                },
                bandWidthInkbps: sendBandWidth.toFixed(2),
                resolution: resolution
            };
        }
        /**
         * Try to create a loopback peer connection using relay server
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents webrtc audio video call quality stats objects
         */

    }, {
        key: "_mayBeStartStreaming",
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.rtcstat = {
                'audio': {
                    'packetsent': -1,
                    'packetloss': -1,
                    'rtt': new _stats.StatisticsAggregate(),
                    'bandwidth_ar': [],
                    'rtt_ar': []
                },
                'video': {
                    'packetsent': -1,
                    'packetloss': -1,
                    'rtt': new _stats.StatisticsAggregate(),
                    'bandwidth_ar': [],
                    'rtt_ar': []
                },
                'stats': undefined
            };

            return new Promise(function (resolve, reject) {
                if (typeof self.runtest_id != 'undefined') {
                    clearTimeout(self.runtest_id);
                    self.runtest_id = undefined;
                }

                self.runtest_id = setTimeout(function () {
                    var avQoS = self._collect_stat.apply(self);
                    console.log('-> ', avQoS);
                    resolve(avQoS);
                }, self.UPTO * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        }
        /**
         * Provides Audio Video call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
         */

    }, {
        key: "testQoS",
        value: function testQoS(progress, log) {
            var self = this;
            self.progress = progress;
            self.log = log;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
            self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

            var constraints = {
                audio: true,
                video: {
                    "width": {
                        "max": "640"
                    },
                    "height": {
                        "max": "480"
                    },
                    "frameRate": {
                        "max": "30"
                    }
                }
            };

            return new Promise(function (resolve, reject) {
                //get user media
                self.util.doGetUserMedia(constraints, function (stream) {
                    self.pc1.addStream(stream);
                    self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                        self._closePC();
                        resolve(success);
                    }).catch(function (err) {
                        self._closePC();
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            });
        }
    }]);

    return ChromeVideoQoS;
}();

exports.ChromeVideoQoS = ChromeVideoQoS;

},{"../lib/stats":6,"../lib/utils":7}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DataThroughput = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require('../lib/utils');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Provides Data Throughput stats.
 * Try to create a Datachannel only loop-back connection using relay-only connection type.
 * Exchange data between two peers for certain period of time, and
 * calculate data transmission rate, packet loss percentage to get data throughput matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
var DataThroughput = function () {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    function DataThroughput(configs) {
        _classCallCheck(this, DataThroughput);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;
        self.dc1 = undefined;

        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.stats = {
            sendByte: 0,
            recvByte: 0,
            lastRecvByte: 0,
            lastTimeStamp: 0,
            bitrates: []
        };
        self.sender_taskid = undefined;
        self.runtest_id = undefined;
        self.sampleData = undefined;
        self.bufferSize = 1024;
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
    }
    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(DataThroughput, [{
        key: '_getRemoteTurn',
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }
        /**
         * Generate data byte
         * @param {number} size - Data chunk size.
         * @returns {Object} Byte object of fixed size
         */

    }, {
        key: '_generate_data',
        value: function _generate_data(size) {
            var self = this;
            for (var i = 0; i < size; i += 1) {
                self.sampleData += 'e';
            }
        }
        /**
         * Create peer connection object
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: '_createPC',
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }
        /**
         * Create Datachannel object
         * @param {Object} peerConnection - PeerConnection object
         * @returns {Object} DataChannel with associated peer connection
         */

    }, {
        key: '_createDC',
        value: function _createDC(peerConnection) {
            var self = this;
            return peerConnection.createDataChannel(null);
        }
        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: '_closePC',
        value: function _closePC() {
            var self = this;
            self.pc1.close();
            self.pc2.close();
            self.pc1 = null;
            self.pc2 = null;

            if (self.sender_taskid !== undefined) {
                clearInterval(self.sender_taskid);
                self.sender_taskid = undefined;
            }
            if (self.runtest_id != undefined) {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }
        }
        /**
         * Register ICE events
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: '_registerIceEvenets',
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
        }
        /**
         * Try to send data using data channel object
         * @param {Object} dc - Datachannel object
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: '_maybedosend',
        value: function _maybedosend(dc, self) {
            self.ITR += 1;
            self.progress(Math.round(100. * self.ITR) / self.UPTO);

            while (dc.bufferedAmount < self.bufferSize) {
                dc.send(self.sampleData);
                self.stats.sendByte += self.bufferSize;
            }
        }
        /**
         * Data channel receiver callback
         * @param {Object} evt - Data channel message object
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: '_on_data',
        value: function _on_data(evt, self) {
            self.stats.recvByte += evt.data.length;
            var now = new Date();

            if (now - self.stats.lastTimeStamp >= 1 * 1000) {
                var bitrate = (self.stats.recvByte - self.stats.lastRecvByte) / (now - self.stats.lastTimeStamp);
                bitrate = (Math.round(bitrate * 1000 * 8) / 1000).toFixed(2);
                if (bitrate > 0) {
                    self.log('Info : current transmitting rate ' + bitrate + " kbps");
                    self.stats.bitrates.push(parseFloat(bitrate));
                }
                self.stats.lastRecvByte = self.stats.recvByte;
                self.stats.lastTimeStamp = now;
            }
        }
        /**
         * Data channel event register
         * @param {Object} dc - DataChannel the loopback session
         * @param {Object} pc - Associated peer connection with given data channel
         */

    }, {
        key: '_registerDCEvenets',
        value: function _registerDCEvenets(dc, pc) {
            var self = this;
            dc.addEventListener('open', function (e) {
                if (self.sender_taskid !== undefined) {
                    clearInterval(self.sender_taskid);
                    self.sender_taskid = undefined;
                }
                console.log('on data channel opened');
                self.sender_taskid = setInterval(function () {
                    self._maybedosend(dc, self);
                }, 1 * 1000);
            });

            pc.addEventListener('datachannel', function (e) {
                var remoteDC = e.channel;
                remoteDC.addEventListener('message', function (evt) {
                    self._on_data(evt, self);
                });
            });
        }
        /**
         * Calculate mean bitrate
         *
         * @returns {Number} mean bitrate of the session.
         */

    }, {
        key: '_calculate_mean_bitrate',
        value: function _calculate_mean_bitrate() {
            var self = this;
            if (self.stats.bitrates.length <= 0) {
                return 0;
            }

            self.stats.bitrates.sort();
            var pivot = Math.floor(self.stats.bitrates.length / 2);
            var meanvalue = 0;
            meanvalue += parseFloat(self.stats.bitrates[pivot]);

            if (self.stats.bitrates.length % 2 == 0) {
                meanvalue += parseFloat(self.stats.bitrates[pivot - 1]);
                meanvalue = Math.floor(meanvalue / 2);
            }
            return meanvalue;
        }

        /**
         * Try to create a loopback peer connection using relay server
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
         */

    }, {
        key: '_mayBeStartStreaming',
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.stats = {
                sendByte: 0,
                recvByte: 0,
                lastRecvByte: 0,
                lastTimeStamp: 0,
                bitrates: [],
                meanbitrate: 0
            };
            self.ITR = 0;
            self._generate_data(self.bufferSize);

            return new Promise(function (resolve, reject) {
                if (self.runtest_id != undefined) {
                    clearTimeout(self.runtest_id);
                    self.runtest_id = undefined;
                }
                self.runtest_id = setTimeout(function () {
                    self.stats.meanbitrate = self._calculate_mean_bitrate();
                    resolve(self.stats);
                }, self.UPTO * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        }

        /**
         * Provides Data Throughput stats
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
         */

    }, {
        key: 'dataThroughputTest',
        value: function dataThroughputTest(progress, log) {
            progress = progress || function () {};
            log = log || function () {};

            var self = this;

            self.progress = progress;
            self.log = log;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
            self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

            self.dc1 = self._createDC(self.pc1);
            self._registerDCEvenets(self.dc1, self.pc2);

            return new Promise(function (resolve, reject) {
                self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                    self._closePC();
                    resolve(success);
                }).catch(function (err) {
                    self._closePC();
                    reject(err);
                });
            });
        }
    }]);

    return DataThroughput;
}();

exports.DataThroughput = DataThroughput;

},{"../lib/utils":7}],13:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MozAudioQoS = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require("../lib/utils");

var _stats = require("../lib/stats");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Provides Mozilla specific Audio only call quality matrices.
 * Try to create a Audio only loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate audio bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
var MozAudioQoS = function () {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    function MozAudioQoS(configs) {
        _classCallCheck(this, MozAudioQoS);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'packetsent': -1,
            'packetloss': -1,
            'rtt': new _stats.StatisticsAggregate(),
            'bandwidth_ar': [],
            'rtt_ar': [],
            'bytesSent': -1,
            'bandwidth': -1
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
        self.interval_id = undefined;
    }

    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(MozAudioQoS, [{
        key: "_getRemoteTurn",
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }

        /**
         * Create peer connection object
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: "_createPC",
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }

        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: "_closePC",
        value: function _closePC() {
            console.log('closing peer connection');
            var self = this;
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            if (typeof self.interval_id != 'undefined') {
                clearInterval(self.interval_id);
                self.interval_id = undefined;
            }
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            if (typeof self.pc1 != 'undefined') {
                self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                    track.stop();
                });
                self.pc1.close();
                self.pc1 = undefined;
            }
            if (typeof self.pc2 != 'undefined') {
                self.pc2.close();
                self.pc2 = undefined;
            }
        }

        /**
         * Collect webRTC matrices for audio only loop-back call.
         * @param {Object} result - webRTC stats object
         * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: "_goStats",
        value: function _goStats(pc1, pc2, lc, rc, self) {
            _getStats(pc1, lc, function (report) {
                for (var i in report) {
                    (function (indx) {
                        var currentReport = report[indx];
                        if (!!navigator.mozGetUserMedia) {
                            switch (currentReport.type) {
                                case 'outboundrtp':
                                    (function (_report) {

                                        var bytesSent = parseFloat(_.get(_report, "bytesSent", 0));
                                        var bitrate = bytesSent - parseFloat(self.rtcstat.bytesSent);
                                        var bandwidth = 0.008 * bitrate;

                                        if (bandwidth > 1) {
                                            self.rtcstat.packetsent = parseFloat(_.get(_report, "packetsSent", 0));
                                            self.rtcstat.bandwidth = bandwidth;
                                            self.rtcstat.bandwidth_ar.push(bandwidth);
                                            self.rtcstat.bytesSent = bytesSent;

                                            self.ITR += 1;
                                            self.progress(Math.round(100. * self.ITR) / self.UPTO);
                                            self.log('Info : current audio transmitting rate ' + bandwidth.toFixed(2) + " kbps");
                                        }
                                    })(currentReport);
                                    break;
                                case 'inboundrtp':
                                    (function (_report) {
                                        var rtt = parseFloat(_.get(_report, "roundTripTime", 0));
                                        self.rtcstat.rtt.add(_report.timestamp, rtt);
                                        self.rtcstat.rtt_ar.push(rtt);
                                        console.log('rtt -> ', rtt);
                                    })(currentReport);
                                    break;
                            }
                        }
                    })(i);
                }
                _getStats(pc2, rc, function (report) {
                    for (var i in report) {
                        (function (indx) {
                            var currentReport = report[indx];
                            (function (_report) {
                                if (!!navigator.mozGetUserMedia) {
                                    switch (_report.type) {
                                        case 'outboundrtp':
                                            break;
                                        case 'inboundrtp':
                                            var packetsLost = parseFloat(_.get(_report, "packetsLost", 0));
                                            self.rtcstat.packetloss = packetsLost;
                                            break;
                                    }
                                }
                            })(currentReport);
                        })(i);
                    }
                }, function (error) {
                    console.error(error);
                });
                // console.log(self.rtcstat);
            }, function (error) {
                console.error(error);
            });
        }

        /**
         * Register ICE events
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: "_registerIceEvenets",
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
            pc2.addEventListener('addstream', function (e) {
                var localSelector = pc1.getLocalStreams()[0].getAudioTracks()[0];
                var remoteSelector = self.pc2.getRemoteStreams()[0].getAudioTracks()[0];
                self.interval_id = setInterval(function () {
                    self._goStats(pc1, pc2, localSelector, remoteSelector, self);
                }, 1 * 1000);
            });
        }

        /**
         * Collect, finalize audio only loopback call stat after completing the full session.
         *
         * @returns {Object} Audio only call quality control matrices
         */

    }, {
        key: "_collect_stat",
        value: function _collect_stat() {
            var self = this;

            function __getstat(data) {
                return {
                    avgRTT: data.rtt.getAverage(),
                    maxRTT: data.rtt.getMax(),
                    packetLoss: parseFloat(100.0 * data.packetloss / data.packetsent).toFixed(2),
                    rtt_ar: data.rtt_ar,
                    bandwidth_ar: data.bandwidth_ar
                };
            }

            return {
                data: __getstat(self.rtcstat),
                bandWidthInkbps: self.rtcstat.bandwidth
            };
        }

        /**
         * Try to create a loopback peer connection using relay server
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents webrtc audio only call quality stats objects
         */

    }, {
        key: "_mayBeStartStreaming",
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.rtcstat = {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1
            };
            self.ITR = 0;
            return new Promise(function (resolve, reject) {
                if (typeof self.runtest_id != 'undefined') {
                    clearTimeout(self.runtest_id);
                    self.runtest_id = undefined;
                }

                self.runtest_id = setTimeout(function () {
                    var audioQoS = self._collect_stat.apply(self);
                    resolve(audioQoS);
                }, self.UPTO * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        }

        /**
         * Provides audio only call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
         */

    }, {
        key: "testQoS",
        value: function testQoS(progress, log) {
            progress = progress || function () {};
            log = log || function () {};

            var self = this;
            self.progress = progress;
            self.log = log;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
            self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

            var constraints = {
                audio: true,
                video: false
            };

            return new Promise(function (resolve, reject) {
                //get user media
                self.util.doGetUserMedia(constraints, function (stream) {
                    self.pc1.addStream(stream);
                    self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                        self._closePC();
                        resolve(success);
                    }).catch(function (err) {
                        self._closePC();
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            });
        }
    }]);

    return MozAudioQoS;
}();

exports.MozAudioQoS = MozAudioQoS;

},{"../lib/stats":6,"../lib/utils":7}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MozVideoQoS = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = require("../lib/utils");

var _stats = require("../lib/stats");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Provides Mozilla firefox compatible Audio Video  call quality matrices.
 * Try to create a Audio+Video loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate AV bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
var MozVideoQoS = function () {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    function MozVideoQoS(configs) {
        _classCallCheck(this, MozVideoQoS);

        var self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = { 'iceServers': [] };
        self.util = new _utils.Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1
            },
            'video': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new _stats.StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1
            },
            'resolution': { width: 0, height: 0 }
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password']
        };
        self.interval_id = undefined;
    }

    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */


    _createClass(MozVideoQoS, [{
        key: "_getRemoteTurn",
        value: function _getRemoteTurn(isUDP) {
            var self = this;
            var iceServer = _.clone(self.turnDetails, true);
            if (isUDP) {
                iceServer.urls = iceServer.urls + "?transport=udp";
            } else {
                iceServer.urls = iceServer.urls + "?transport=tcp";
            }
            self.config.iceServers.push(iceServer);
            return self.config;
        }

        /**
         * Create peer connection object
         * @param {Object} iceConfig - Turn ICE configuration
         * @returns {Object} PeerConnection object
         */

    }, {
        key: "_createPC",
        value: function _createPC(iceConfig) {
            var self = this;
            return new RTCPeerConnection(iceConfig, null);
        }

        /**
         * Close peer connection, and free resources.
         *
         */

    }, {
        key: "_closePC",
        value: function _closePC() {
            console.log('closing peer connection');
            var self = this;

            if (typeof self.interval_id != 'undefined') {
                clearInterval(self.interval_id);
                self.interval_id = undefined;
            }

            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            if (typeof self.pc1 != 'undefined') {
                self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                    track.stop();
                });
                self.pc1.close();
                self.pc1 = undefined;
            }
            if (typeof self.pc2 != 'undefined') {
                self.pc2.close();
                self.pc2 = undefined;
            }
        }

        /**
         * Collect webRTC matrices for audio video loop-back call.
         * @param {Object} result - webRTC stats object
         * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
         * @param {Object} self - Scope object of callee function
         */

    }, {
        key: "_goStats",
        value: function _goStats(pc1, pc2, alc, arc, vlc, vrc, self) {
            function ___collect_internals(stat_obj, outbound, is_local, _report) {
                if (outbound == true) {
                    var bytesSent = parseFloat(_.get(_report, "bytesSent", 0));
                    var bitrate = bytesSent - parseFloat(stat_obj.bytesSent);
                    var bandwidth = 0.008 * bitrate;
                    if (bandwidth > 1) {
                        stat_obj.packetsent = parseFloat(_.get(_report, "packetsSent", 0));
                        stat_obj.bandwidth = bandwidth;
                        stat_obj.bandwidth_ar.push(bandwidth);
                        stat_obj.bytesSent = bytesSent;
                    }
                } else {
                    if (is_local == true) {
                        var rtt = parseFloat(_.get(_report, "roundTripTime", 0));
                        stat_obj.rtt.add(_report.timestamp, rtt);
                        stat_obj.rtt_ar.push(rtt);
                    } else {
                        var packetsLost = parseFloat(_.get(_report, "packetsLost", 0));
                        stat_obj.packetloss = packetsLost;
                    }
                }
            }

            _getStats(pc1, alc, function (report) {
                for (var i in report) {
                    (function (indx) {
                        var currentReport = report[indx];
                        if (!!navigator.mozGetUserMedia) {
                            switch (currentReport.type) {
                                case 'outboundrtp':
                                    (function (_report) {
                                        ___collect_internals(self.rtcstat.audio, true, true, _report);
                                    })(currentReport);
                                    break;
                                case 'inboundrtp':
                                    (function (_report) {
                                        ___collect_internals(self.rtcstat.audio, false, true, _report);
                                    })(currentReport);
                                    break;
                            }
                        }
                    })(i);
                }
                _getStats(pc2, arc, function (report) {
                    for (var i in report) {
                        (function (indx) {
                            var currentReport = report[indx];
                            (function (_report) {
                                if (!!navigator.mozGetUserMedia) {
                                    switch (_report.type) {
                                        case 'outboundrtp':
                                            break;
                                        case 'inboundrtp':
                                            ___collect_internals(self.rtcstat.audio, false, false, _report);
                                            break;
                                    }
                                }
                            })(currentReport);
                        })(i);
                    }
                }, function (error) {
                    console.error(error);
                });
            }, function (error) {
                console.error(error);
            });

            _getStats(pc1, vlc, function (report) {
                for (var i in report) {
                    (function (indx) {
                        var currentReport = report[indx];
                        if (!!navigator.mozGetUserMedia) {
                            switch (currentReport.type) {
                                case 'outboundrtp':
                                    (function (_report) {
                                        ___collect_internals(self.rtcstat.video, true, true, _report);
                                    })(currentReport);
                                    break;
                                case 'inboundrtp':
                                    (function (_report) {
                                        ___collect_internals(self.rtcstat.video, false, true, _report);
                                    })(currentReport);
                                    break;
                            }
                        }
                    })(i);
                }
                _getStats(pc2, vrc, function (report) {
                    for (var i in report) {
                        (function (indx) {
                            var currentReport = report[indx];
                            (function (_report) {
                                if (!!navigator.mozGetUserMedia) {
                                    switch (_report.type) {
                                        case 'outboundrtp':
                                            break;
                                        case 'inboundrtp':
                                            ___collect_internals(self.rtcstat.video, false, false, _report);
                                            break;
                                    }
                                }
                            })(currentReport);
                        })(i);
                    }
                }, function (error) {
                    console.error(error);
                });
            }, function (error) {
                console.error(error);
            });
            console.log(self.rtcstat);
            var avbandwidth = self.rtcstat.audio.bandwidth + self.rtcstat.video.bandwidth;
            if (avbandwidth > 1) {
                self.ITR += 1;
                self.progress(Math.round(100. * self.ITR) / self.UPTO);
                self.log('Info : current transmitting rate ' + avbandwidth.toFixed(2) + " kbps");
            }
        }

        /**
         * Register ICE events
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @param {Function} isGood - Supported Relay candidate type
         */

    }, {
        key: "_registerIceEvenets",
        value: function _registerIceEvenets(pc1, pc2, isGood) {
            var self = this;
            pc1.addEventListener('icecandidate', function (e) {
                if (e.candidate) {
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (isGood(parsed)) {
                        pc2.addIceCandidate(e.candidate);
                    }
                }
            });
            pc2.addEventListener('addstream', function (e) {
                var localAudioSelector = pc1.getLocalStreams()[0].getAudioTracks()[0];
                var remoteAudioSelector = pc2.getRemoteStreams()[0].getAudioTracks()[0];

                var localVideoSelector = pc1.getLocalStreams()[0].getVideoTracks()[0];
                var remoteVideoSelector = pc2.getRemoteStreams()[0].getVideoTracks()[0];
                self.interval_id = setInterval(function () {
                    self._goStats(pc1, pc2, localAudioSelector, remoteAudioSelector, localVideoSelector, remoteVideoSelector, self);
                }, 1 * 1000);
            });
        }

        /**
         * Collect, finalize audio video loopback call stat after completing the full session.
         *
         * @returns {Object} Audio Video call quality control matrices
         */

    }, {
        key: "_collect_stat",
        value: function _collect_stat() {
            var self = this;

            function __getstat(data) {
                var avgRTT = data.rtt.getAverage();
                var maxRTT = data.rtt.getMax();
                var packetLossPercentage = 100.0 * data.packetloss / data.packetsent;

                return {
                    avgRTT: avgRTT.toFixed(2),
                    maxRTT: maxRTT.toFixed(2),
                    packetLoss: packetLossPercentage.toFixed(2),
                    rtt_ar: data.rtt_ar,
                    bandwidth_ar: data.bandwidth_ar
                };
            }
            var avbandwidth = self.rtcstat.audio.bandwidth + self.rtcstat.video.bandwidth;
            return {
                audio: {
                    data: __getstat(self.rtcstat.audio)
                },
                video: {
                    data: __getstat(self.rtcstat.video)
                },
                bandWidthInkbps: avbandwidth.toFixed(2),
                resolution: self.rtcstat.resolution
            };
        }

        /**
         * Try to create a loopback peer connection using relay server
         * @param {Object} pc1 - First peer connection in the loopback session
         * @param {Object} pc2 - Second peer connection in the loopback session
         * @returns {Promise} A promise object that represents webrtc audio video call quality stats objects
         */

    }, {
        key: "_mayBeStartStreaming",
        value: function _mayBeStartStreaming(pc1, pc2) {
            var self = this;
            self.rtcstat = {
                'audio': {
                    'packetsent': -1,
                    'packetloss': -1,
                    'rtt': new _stats.StatisticsAggregate(),
                    'bandwidth_ar': [],
                    'rtt_ar': [],
                    'bytesSent': -1,
                    'bandwidth': -1
                },
                'video': {
                    'packetsent': -1,
                    'packetloss': -1,
                    'rtt': new _stats.StatisticsAggregate(),
                    'bandwidth_ar': [],
                    'rtt_ar': [],
                    'bytesSent': -1,
                    'bandwidth': -1
                },
                'resolution': self.rtcstat.resolution
            };

            return new Promise(function (resolve, reject) {
                if (typeof self.runtest_id != 'undefined') {
                    clearTimeout(self.runtest_id);
                    self.runtest_id = undefined;
                }

                self.runtest_id = setTimeout(function () {
                    var avQoS = self._collect_stat.apply(self);
                    console.log('-> ', avQoS);
                    resolve(avQoS);
                }, self.UPTO * 1000);

                pc1.createOffer().then(function (offer) {
                    pc1.setLocalDescription(offer);
                    pc2.setRemoteDescription(offer);
                    pc2.createAnswer().then(function (answer) {
                        pc2.setLocalDescription(answer);
                        pc1.setRemoteDescription(answer);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            });
        }

        /**
         * Provides Audio Video call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
         */

    }, {
        key: "testQoS",
        value: function testQoS(progress, log) {
            var self = this;
            self.progress = progress;
            self.log = log;

            var iceConfig = self._getRemoteTurn(true);
            self.pc1 = self._createPC(iceConfig);
            self.pc2 = self._createPC(iceConfig);

            self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
            self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

            var constraints = {
                audio: true,
                video: {
                    "width": {
                        "max": "640"
                    },
                    "height": {
                        "max": "480"
                    },
                    "frameRate": {
                        "max": "30"
                    }
                }
            };

            self.rtcstat.resolution = { width: 640, height: 480 };
            return new Promise(function (resolve, reject) {
                //get user media
                self.util.doGetUserMedia(constraints, function (stream) {
                    self.pc1.addStream(stream);
                    self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                        self._closePC();
                        resolve(success);
                    }).catch(function (err) {
                        self._closePC();
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                });
            });
        }
    }]);

    return MozVideoQoS;
}();

exports.MozVideoQoS = MozVideoQoS;

},{"../lib/stats":6,"../lib/utils":7}],15:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _availabilitycheck = require('../precalltest/networktest/availabilitycheck');

var _browser = require('../precalltest/browsertest/browser');

var _hardware = require('../precalltest/hardwaretest/hardware');

var _networkcheck = require('./networktest/networkcheck');

var _connectivitytest = require('./connectivitytest/connectivitytest');

var _datathroughtputtest = require('./throughputtest/datathroughtputtest');

var _chromeaudioqos = require('./throughputtest/chromeaudioqos');

var _mozaudioqos = require('./throughputtest/mozaudioqos');

var _chromevideoqos = require('./throughputtest/chromevideoqos');

var _mozvideoqos = require('./throughputtest/mozvideoqos');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @class Principle SDK wrapper class.
 * Client will only have access to this class, and communicate internal classes through it.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

var WebRTCPreCallTest = function () {
    /** @constructs
     */
    function WebRTCPreCallTest() {
        _classCallCheck(this, WebRTCPreCallTest);

        var self = this;
    }

    /**
     * Provides online/offline status
     * @returns {bool} Verify if you are online, or offline
     */


    _createClass(WebRTCPreCallTest, [{
        key: 'isOnline',
        value: function isOnline() {
            var self = this;
            var network = new _availabilitycheck.AvailabilityCheck();
            return network.isNetworkAvailable();
        }

        /**
         * Get the current browser details
         * @returns {Object} represents browserType, version, and supportWebRTC details of the browser
         */

    }, {
        key: 'browserDetails',
        value: function browserDetails() {
            var browser = new _browser.Browser();
            return new Promise(function (resolve, reject) {
                resolve(browser.getDetails());
            });
        }

        /**
         * Returns list of available audio or video input devices
         * @param {bool} isAudio - Check if you need audio, or video hardware details
         * @returns {Promise} Promise object represents list of available audio or video input devices
         */

    }, {
        key: 'hardwareDetails',
        value: function hardwareDetails(isAudio) {
            var hardware = new _hardware.Hardware();
            return hardware.supportDetails(isAudio);
        }

        /**
         * Returns list of supported video resolution
         * @param {function} progress binder to show progress in UI
         * @param {function} log binder to log in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} Promise object represents list of supported video resolution.
         */

    }, {
        key: 'resolutionSupport',
        value: function resolutionSupport(progress, log, config) {
            var hardware = new _hardware.Hardware(config);
            return hardware.resolutionTest(progress);
        }

        /**
         * Get network connectivity details
         * @param {function} progress binder to show progress in UI
         * @param {function} log binder to log in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
         */

    }, {
        key: 'networkTest',
        value: function networkTest(progress, log, config) {
            var networktest = new _networkcheck.Network(config);
            return networktest.getNetworkDetail(progress, log);
        }

        /**
         * Check connectivity detail
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} Promise object that contains Relay, Reflexive, Host only connection support details.
         */

    }, {
        key: 'connectivityTest',
        value: function connectivityTest(progress, log, config) {
            var connectivity = new _connectivitytest.Connectivity(config);
            return connectivity.checkConnectivity(progress, log);
        }

        /**
         * Provides Data Throughput stats
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
         */

    }, {
        key: 'dataThroughputTest',
        value: function dataThroughputTest(progress, log, config) {
            var throughput = new _datathroughtputtest.DataThroughput(config);
            return throughput.dataThroughputTest(progress, log);
        }

        /**
         * Provides chrome audio only call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
         */

    }, {
        key: 'chromeAudioOnlyQoS',
        value: function chromeAudioOnlyQoS(progress, log, config) {
            var audioQoS = new _chromeaudioqos.ChromeAudioQoS(config);
            return audioQoS.testQoS(progress, log);
        }

        /**
         * Provides mozilla audio only call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
         */

    }, {
        key: 'mozAudioOnlyQoS',
        value: function mozAudioOnlyQoS(progress, log, config) {
            var audioQoS = new _mozaudioqos.MozAudioQoS(config);
            return audioQoS.testQoS(progress, log);
        }

        /**
         * Provides specific audio only call quality matrices. Currently only support mozilla firefox, and google chrome
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
         */

    }, {
        key: 'audioOnlyQoS',
        value: function audioOnlyQoS(progress, log, config) {
            var self = this;
            if (adapter.browserDetails.browser == "firefox") {
                return self.mozAudioOnlyQoS(progress, log, config);
            } else {
                return self.chromeAudioOnlyQoS(progress, log, config);
            }
        }

        /**
         * Provides Chrome specific Audio Video call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
         */

    }, {
        key: 'chromeVideoOnlyQoS',
        value: function chromeVideoOnlyQoS(progress, log, config) {
            var videoQoS = new _chromevideoqos.ChromeVideoQoS(config);
            return videoQoS.testQoS(progress, log);
        }

        /**
         * Provides Firefox specific Audio Video call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
         */

    }, {
        key: 'mozVideoOnlyQoS',
        value: function mozVideoOnlyQoS(progress, log, config) {
            var videoQoS = new _mozvideoqos.MozVideoQoS(config);
            return videoQoS.testQoS(progress, log);
        }

        /**
         * Provides Audio Video call quality matrices
         * @param {function} progress - binder to show progress in UI
         * @param {function} log - binder to show logs in UI
         * @param {Object} configs - Test Duration, Turn details
         * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
         */

    }, {
        key: 'videoOnlyQoS',
        value: function videoOnlyQoS(progress, log, config) {
            var self = this;
            if (adapter.browserDetails.browser == "firefox") {
                return self.mozVideoOnlyQoS(progress, log, config);
            } else {
                return self.chromeVideoOnlyQoS(progress, log, config);
            }
        }
    }]);

    return WebRTCPreCallTest;
}();

module.exports = WebRTCPreCallTest;

},{"../precalltest/browsertest/browser":1,"../precalltest/hardwaretest/hardware":4,"../precalltest/networktest/availabilitycheck":8,"./connectivitytest/connectivitytest":2,"./networktest/networkcheck":9,"./throughputtest/chromeaudioqos":10,"./throughputtest/chromevideoqos":11,"./throughputtest/datathroughtputtest":12,"./throughputtest/mozaudioqos":13,"./throughputtest/mozvideoqos":14}]},{},[15])(15)
});