'use strict';
import {Utils} from "../lib/utils";
import {StatisticsAggregate} from "../lib/stats";

/**
 * @class Provides Mozilla firefox compatible Audio Video  call quality matrices.
 * Try to create a Audio+Video loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate AV bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
class MozVideoQoS {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    constructor(configs) {
        let self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = {'iceServers': []};
        self.util = new Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1,
            },
            'video': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1,
            },
            'resolution': {width: 0, height: 0},
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat(configs['test-timeout']);
        self.turnDetails = {
            urls: configs['turn-url'],
            username: configs['turn-uname'],
            credential: configs['turn-password'],
        };
        self.interval_id = undefined;
    }

    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */
    _getRemoteTurn(isUDP) {
        let self = this;
        let iceServer = _.clone(self.turnDetails, true);
        if (isUDP) {
            iceServer.urls = (iceServer.urls + "?transport=udp")
        } else {
            iceServer.urls = (iceServer.urls + "?transport=tcp")
        }
        self.config.iceServers.push(iceServer);
        return self.config;
    }

    /**
     * Create peer connection object
     * @param {Object} iceConfig - Turn ICE configuration
     * @returns {Object} PeerConnection object
     */
    _createPC(iceConfig) {
        let self = this;
        return new RTCPeerConnection(iceConfig, null);
    }

    /**
     * Close peer connection, and free resources.
     *
     */
    _closePC() {
        console.log('closing peer connection');
        let self = this;

        if (typeof self.interval_id != 'undefined') {
            clearInterval(self.interval_id);
            self.interval_id = undefined;
        }

        if (typeof self.runtest_id != 'undefined') {
            clearTimeout(self.runtest_id);
            self.runtest_id = undefined;
        }

        if (typeof self.pc1 != 'undefined') {
            self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                track.stop();
            });
            self.pc1.close();
            self.pc1 = undefined;
        }
        if (typeof self.pc2 != 'undefined') {
            self.pc2.close();
            self.pc2 = undefined;
        }

    }

    /**
     * Collect webRTC matrices for audio video loop-back call.
     * @param {Object} result - webRTC stats object
     * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
     * @param {Object} self - Scope object of callee function
     */
    _goStats(pc1, pc2, alc, arc, vlc, vrc, self) {
        function ___collect_internals(stat_obj, outbound, is_local, _report) {
            if (outbound == true) {
                const bytesSent = parseFloat(_.get(_report, "bytesSent", 0));
                const bitrate = (bytesSent - parseFloat(stat_obj.bytesSent));
                const bandwidth = 0.008 * bitrate;
                if (bandwidth > 1) {
                    stat_obj.packetsent = parseFloat(_.get(_report, "packetsSent", 0));
                    stat_obj.bandwidth = bandwidth;
                    stat_obj.bandwidth_ar.push(bandwidth);
                    stat_obj.bytesSent = bytesSent;
                }
            } else {
                if (is_local == true) {
                    const rtt = parseFloat(_.get(_report, "roundTripTime", 0));
                    stat_obj.rtt.add(_report.timestamp, rtt);
                    stat_obj.rtt_ar.push(rtt);
                } else {
                    const packetsLost = parseFloat(_.get(_report, "packetsLost", 0));
                    stat_obj.packetloss = packetsLost;
                }
            }
        }

        _getStats(pc1, alc, function (report) {
            for (var i in report) {
                (function (indx) {
                    var currentReport = report[indx];
                    if (!!navigator.mozGetUserMedia) {
                        switch (currentReport.type) {
                            case 'outboundrtp':
                                (function (_report) {
                                    ___collect_internals(self.rtcstat.audio, true, true, _report);
                                })(currentReport);
                                break;
                            case 'inboundrtp':
                                (function (_report) {
                                    ___collect_internals(self.rtcstat.audio, false, true, _report);
                                })(currentReport);
                                break;
                        }
                    }
                })(i);
            }
            _getStats(pc2, arc, function (report) {
                for (var i in report) {
                    (function (indx) {
                        var currentReport = report[indx];
                        (function (_report) {
                            if (!!navigator.mozGetUserMedia) {
                                switch (_report.type) {
                                    case 'outboundrtp':
                                        break;
                                    case 'inboundrtp':
                                        ___collect_internals(self.rtcstat.audio, false, false, _report);
                                        break;
                                }
                            }
                        })(currentReport);

                    })(i);

                }
            }, function (error) {
                console.error(error);
            });
        }, function (error) {
            console.error(error);
        });

        _getStats(pc1, vlc, function (report) {
            for (var i in report) {
                (function (indx) {
                    var currentReport = report[indx];
                    if (!!navigator.mozGetUserMedia) {
                        switch (currentReport.type) {
                            case 'outboundrtp':
                                (function (_report) {
                                    ___collect_internals(self.rtcstat.video, true, true, _report);
                                })(currentReport);
                                break;
                            case 'inboundrtp':
                                (function (_report) {
                                    ___collect_internals(self.rtcstat.video, false, true, _report);
                                })(currentReport);
                                break;
                        }
                    }
                })(i);
            }
            _getStats(pc2, vrc, function (report) {
                for (var i in report) {
                    (function (indx) {
                        var currentReport = report[indx];
                        (function (_report) {
                            if (!!navigator.mozGetUserMedia) {
                                switch (_report.type) {
                                    case 'outboundrtp':
                                        break;
                                    case 'inboundrtp':
                                        ___collect_internals(self.rtcstat.video, false, false, _report);
                                        break;
                                }
                            }
                        })(currentReport);

                    })(i);

                }
            }, function (error) {
                console.error(error);
            });
        }, function (error) {
            console.error(error);
        });
        console.log(self.rtcstat);
        const avbandwidth = self.rtcstat.audio.bandwidth + self.rtcstat.video.bandwidth;
        if (avbandwidth > 1) {
            self.ITR += 1;
            self.progress(Math.round(100. * self.ITR) / self.UPTO);
            self.log('Info : current transmitting rate ' + avbandwidth.toFixed(2) + " kbps");
        }
    }

    /**
     * Register ICE events
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @param {Function} isGood - Supported Relay candidate type
     */
    _registerIceEvenets(pc1, pc2, isGood) {
        let self = this;
        pc1.addEventListener('icecandidate', function (e) {
            if (e.candidate) {
                let parsed = self.util.parseCandidate(e.candidate.candidate);
                if (isGood(parsed)) {
                    pc2.addIceCandidate(e.candidate);
                }
            }
        });
        pc2.addEventListener('addstream', function (e) {
            const localAudioSelector = pc1.getLocalStreams()[0].getAudioTracks()[0];
            const remoteAudioSelector = pc2.getRemoteStreams()[0].getAudioTracks()[0];

            const localVideoSelector = pc1.getLocalStreams()[0].getVideoTracks()[0];
            const remoteVideoSelector = pc2.getRemoteStreams()[0].getVideoTracks()[0];
            self.interval_id = setInterval(function () {
                self._goStats(pc1, pc2, localAudioSelector, remoteAudioSelector, localVideoSelector, remoteVideoSelector, self);
            }, 1 * 1000);
        });

    }

    /**
     * Collect, finalize audio video loopback call stat after completing the full session.
     *
     * @returns {Object} Audio Video call quality control matrices
     */
    _collect_stat() {
        let self = this;

        function __getstat(data) {
            const avgRTT = data.rtt.getAverage();
            const maxRTT = data.rtt.getMax();
            const packetLossPercentage = ((100.0 * data.packetloss) / data.packetsent);

            return {
                avgRTT: (avgRTT).toFixed(2),
                maxRTT: (maxRTT).toFixed(2),
                packetLoss: (packetLossPercentage).toFixed(2),
                rtt_ar: data.rtt_ar,
                bandwidth_ar: data.bandwidth_ar,
            }
        }
        const avbandwidth = self.rtcstat.audio.bandwidth + self.rtcstat.video.bandwidth;
        return {
            audio: {
                data: __getstat(self.rtcstat.audio),
            },
            video: {
                data: __getstat(self.rtcstat.video),
            },
            bandWidthInkbps: (avbandwidth).toFixed(2),
            resolution: self.rtcstat.resolution,
        };
    }

    /**
     * Try to create a loopback peer connection using relay server
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @returns {Promise} A promise object that represents webrtc audio video call quality stats objects
     */
    _mayBeStartStreaming(pc1, pc2) {
        let self = this;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1,
            },
            'video': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
                'bytesSent': -1,
                'bandwidth': -1,
            },
            'resolution' : self.rtcstat.resolution,
        };

        return new Promise((resolve, reject) => {
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            self.runtest_id = setTimeout(function () {
                const avQoS = self._collect_stat.apply(self);
                console.log('-> ', avQoS);
                resolve(avQoS);
            }, self.UPTO * 1000);

            pc1.createOffer().then(function (offer) {
                pc1.setLocalDescription(offer);
                pc2.setRemoteDescription(offer);
                pc2.createAnswer().then(function (answer) {
                    pc2.setLocalDescription(answer);
                    pc1.setRemoteDescription(answer);
                });
            }).catch(function (err) {
                reject(err);
            });
        });

    }

    /**
     * Provides Audio Video call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
     */
    testQoS(progress, log) {
        let self = this;
        self.progress = progress;
        self.log = log;

        let iceConfig = self._getRemoteTurn(true);
        self.pc1 = self._createPC(iceConfig);
        self.pc2 = self._createPC(iceConfig);

        self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
        self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

        const constraints = {
            audio: true,
            video: {
                "width": {
                    "max": "640"
                },
                "height": {
                    "max": "480"
                },
                "frameRate": {
                    "max": "30"
                }
            },
        };

        self.rtcstat.resolution = {width: 640, height: 480};
        return new Promise((resolve, reject) => {
            //get user media
            self.util.doGetUserMedia(constraints, function (stream) {
                self.pc1.addStream(stream);
                self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                    self._closePC();
                    resolve(success)
                }).catch(function (err) {
                    self._closePC();
                    reject(err);
                });
            }, function (err) {
                reject(err);
            });
        });

    }
}

export {MozVideoQoS};
