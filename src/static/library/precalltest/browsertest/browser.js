'use strict';

/**
 * @class Gather current browser detail, it's version, and whether the browser support webRTC.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
class Browser {
    /** @constructs */
    constructor() {
        let self = this;
    }

    /**
     * Get the current browser details
     *
     * @returns {Object} represents browserType, version, and supportWebRTC details of the browser
     */
    getDetails() {
        let retval = adapter.browserDetails;
        return {
            browserType: retval.browser,
            version: retval.version,
            supportWebRTC: retval.version == null ? false : true,
        }

    }
}

export {Browser};
