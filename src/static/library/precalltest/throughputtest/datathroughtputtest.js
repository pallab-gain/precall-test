'use strict';
import {Utils} from "../lib/utils";

/**
 * @class Provides Data Throughput stats.
 * Try to create a Datachannel only loop-back connection using relay-only connection type.
 * Exchange data between two peers for certain period of time, and
 * calculate data transmission rate, packet loss percentage to get data throughput matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
class DataThroughput {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    constructor(configs) {
        let self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;
        self.dc1 = undefined;

        self.config = {'iceServers': []};
        self.util = new Utils();
        self.stats = {
            sendByte: 0,
            recvByte: 0,
            lastRecvByte: 0,
            lastTimeStamp: 0,
            bitrates : [],
        };
        self.sender_taskid = undefined;
        self.runtest_id = undefined;
        self.sampleData = undefined;
        self.bufferSize = 1024;
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat( configs['test-timeout'] );
        self.turnDetails = {
            urls:  configs['turn-url'],
            username:  configs['turn-uname'],
            credential:  configs['turn-password'],
        };
    }
    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */
    _getRemoteTurn(isUDP) {
        let self = this;
        let iceServer = _.clone(self.turnDetails,true);
        if (isUDP) {
            iceServer.urls = (iceServer.urls+"?transport=udp")
        } else {
            iceServer.urls = (iceServer.urls+"?transport=tcp")
        }
        self.config.iceServers.push(iceServer);
        return self.config;
    }
    /**
     * Generate data byte
     * @param {number} size - Data chunk size.
     * @returns {Object} Byte object of fixed size
     */
    _generate_data(size) {
        let self = this;
        for (let i = 0; i < size; i += 1) {
            self.sampleData += 'e';
        }
    }
    /**
     * Create peer connection object
     * @param {Object} iceConfig - Turn ICE configuration
     * @returns {Object} PeerConnection object
     */
    _createPC(iceConfig) {
        let self = this;
        return new RTCPeerConnection(iceConfig, null);
    }
    /**
     * Create Datachannel object
     * @param {Object} peerConnection - PeerConnection object
     * @returns {Object} DataChannel with associated peer connection
     */
    _createDC(peerConnection) {
        let self = this;
        return peerConnection.createDataChannel(null);
    }
    /**
     * Close peer connection, and free resources.
     *
     */
    _closePC() {
        let self = this;
        self.pc1.close();
        self.pc2.close();
        self.pc1 = null;
        self.pc2 = null;

        if (self.sender_taskid !== undefined) {
            clearInterval(self.sender_taskid);
            self.sender_taskid = undefined;
        }
        if (self.runtest_id != undefined) {
            clearTimeout(self.runtest_id);
            self.runtest_id = undefined;
        }
    }
    /**
     * Register ICE events
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @param {Function} isGood - Supported Relay candidate type
     */
    _registerIceEvenets(pc1, pc2, isGood) {
        let self = this;
        pc1.addEventListener('icecandidate', function (e) {
            if (e.candidate) {
                let parsed = self.util.parseCandidate(e.candidate.candidate);
                if (isGood(parsed)) {
                    pc2.addIceCandidate(e.candidate);
                }
            }
        });
    }
    /**
     * Try to send data using data channel object
     * @param {Object} dc - Datachannel object
     * @param {Object} self - Scope object of callee function
     */
    _maybedosend(dc, self) {
        self.ITR+=1;
        self.progress( Math.round(100.*self.ITR)/self.UPTO );

        while (dc.bufferedAmount < self.bufferSize) {
            dc.send(self.sampleData);
            self.stats.sendByte += self.bufferSize;
        }
    }
    /**
     * Data channel receiver callback
     * @param {Object} evt - Data channel message object
     * @param {Object} self - Scope object of callee function
     */
    _on_data(evt, self) {
        self.stats.recvByte += evt.data.length;
        const now = new Date();

        if( now - self.stats.lastTimeStamp >= 1*1000) {
            let bitrate = (self.stats.recvByte - self.stats.lastRecvByte)/(now-self.stats.lastTimeStamp);
            bitrate = (Math.round(bitrate * 1000 * 8) / 1000).toFixed(2);
            if (bitrate>0) {
                self.log('Info : current transmitting rate '+bitrate+" kbps");
                self.stats.bitrates.push( parseFloat(bitrate)) ;
            }
            self.stats.lastRecvByte = self.stats.recvByte;
            self.stats.lastTimeStamp = now;
        }
    }
    /**
     * Data channel event register
     * @param {Object} dc - DataChannel the loopback session
     * @param {Object} pc - Associated peer connection with given data channel
     */
    _registerDCEvenets(dc, pc) {
        let self = this;
        dc.addEventListener('open', function (e) {
            if (self.sender_taskid !== undefined) {
                clearInterval(self.sender_taskid);
                self.sender_taskid = undefined;
            }
            console.log('on data channel opened');
            self.sender_taskid = setInterval(function () {
                self._maybedosend(dc, self);
            }, 1 * 1000)
        });

        pc.addEventListener('datachannel', function (e) {
            let remoteDC = e.channel;
            remoteDC.addEventListener('message', function (evt) {
                self._on_data(evt, self);
            });
        });
    }
    /**
     * Calculate mean bitrate
     *
     * @returns {Number} mean bitrate of the session.
     */
    _calculate_mean_bitrate(){
        let self = this;
        if(self.stats.bitrates.length<=0){
            return 0;
        }

        self.stats.bitrates.sort();
        const pivot = Math.floor(self.stats.bitrates.length/2);
        let meanvalue = 0;
        meanvalue+= parseFloat(self.stats.bitrates[pivot]);

        if( self.stats.bitrates.length % 2 == 0){
            meanvalue+= parseFloat(self.stats.bitrates[pivot-1]);
            meanvalue = Math.floor(meanvalue/2)
        }
        return meanvalue;
    }

    /**
     * Try to create a loopback peer connection using relay server
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
     */
    _mayBeStartStreaming(pc1, pc2) {
        let self = this;
        self.stats = {
            sendByte: 0,
            recvByte: 0,
            lastRecvByte: 0,
            lastTimeStamp: 0,
            bitrates : [],
            meanbitrate: 0,
        };
        self.ITR = 0;
        self._generate_data(self.bufferSize);

        return new Promise((resolve, reject) => {
            if (self.runtest_id != undefined) {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }
            self.runtest_id = setTimeout(function () {
                self.stats.meanbitrate = self._calculate_mean_bitrate();
                resolve(self.stats)
            }, self.UPTO * 1000);

            pc1.createOffer().then(function (offer) {
                pc1.setLocalDescription(offer);
                pc2.setRemoteDescription(offer);
                pc2.createAnswer().then(function (answer) {
                    pc2.setLocalDescription(answer);
                    pc1.setRemoteDescription(answer);
                });
            }).catch(function (err) {
                reject(err);
            });
        });

    }

    /**
     * Provides Data Throughput stats
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
     */
    dataThroughputTest(progress,log) {
        progress = progress||function(){};
        log = log||function(){};

        let self = this;

        self.progress = progress;
        self.log = log;

        let iceConfig = self._getRemoteTurn(true);
        self.pc1 = self._createPC(iceConfig);
        self.pc2 = self._createPC(iceConfig);

        self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
        self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

        self.dc1 = self._createDC(self.pc1);
        self._registerDCEvenets(self.dc1, self.pc2);

        return new Promise((resolve, reject) => {
            self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                self._closePC();
                resolve(success)
            }).catch(function (err) {
                self._closePC();
                reject(err);
            })
        });

    }
}

export {DataThroughput};
