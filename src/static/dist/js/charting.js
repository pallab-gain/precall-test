function audioBandwidthChart(_data){
    Highcharts.chart('audio-container-bandwidth', {
        chart: {
            type: 'spline'
        },
        title: { text: 'Audio Bandwidth' } ,
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            title: {
                text: 'Audio bandwidth (kb/s)'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ // Light air
                from: 0.0,
                to: 25.0,
                color: '#EDD0D9',
                label: {
                    text: 'Poor',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Light breeze
                from: 25.0,
                to: 30.0,
                color: '#BBE7DB',
                label: {
                    text: 'Accepted',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Gentle breeze
                from: 30.,
                to: 1000.,
                color: '#D3E4BA',
                label: {
                    text: 'Excellent',
                    style: {
                        color: '#09181D'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' kb/s'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1000, // one hour
                pointStart: Date.now(),
            }
        },
        series: [{
            name: 'Audio Bandwidth',
            data: _data

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
}

function avBandwidthChart(_data){
    Highcharts.chart('av-container-bandwidth', {
        chart: {
            type: 'spline'
        },
        title: { text: 'AV Bandwidth' } ,
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            title: {
                text: 'Video bandwidth (kb/s)'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ // Light air
                from: 0.0,
                to: 350.0,
                color: '#EDD0D9',
                label: {
                    text: 'Poor',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Light breeze
                from: 350.0,
                to: 600.0,
                color: '#BBE7DB',
                label: {
                    text: 'Accepted',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Gentle breeze
                from: 600.,
                to: 5000.,
                color: '#D3E4BA',
                label: {
                    text: 'Excellent',
                    style: {
                        color: '#09181D'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' kb/s'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1000, // one hour
                pointStart: Date.now(),
            }
        },
        series: [{
            name: 'Video Bandwidth',
            data: _data

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
}

function dataBandwidthChart(_data){
    Highcharts.chart('data-container', {
        chart: {
            type: 'spline'
        },
        title: { text: 'Data Bandwidth' } ,
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            title: {
                text: 'Data bandwidth (kb/s)'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
        },
        tooltip: {
            valueSuffix: ' kb/s'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1000, // one hour
                pointStart: Date.now(),
            }
        },
        series: [{
            name: 'Data Bandwidth',
            data: _data

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
}

function audioRTTChart(_data){
    Highcharts.chart('audio-container-rtt', {
        chart: {
            type: 'spline'
        },
        title: { text: 'Audio RTT' } ,
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            title: {
                text: 'Audio RTT (ms)'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ // Light air
                from: 0.0,
                to: 250.0,
                color: '#D3E4BA',
                label: {
                    text: 'Excellent',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Light breeze
                from: 250.0,
                to: 400.0,
                color: '#BBE7DB',
                label: {
                    text: 'Moderate',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Gentle breeze
                from: 400.,
                to: 500.,
                color: '#BBE7DB',
                label: {
                    text: 'Moderate',
                    style: {
                        color: '#09181D'
                    }
                }
            },{ // Gentle breeze
                from: 500.,
                to: 20000.,
                color: '#EDD0D9',
                label: {
                    text: 'Poor',
                    style: {
                        color: '#09181D'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' ms'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1000, // one hour
                pointStart: Date.now(),
            }
        },
        series: [{
            name: 'Audio RTT',
            data: _data

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
}

function avRTTChart(_data){
    Highcharts.chart('av-container-rtt', {
        chart: {
            type: 'spline'
        },
        title: { text: 'Video RTT' } ,
        xAxis: {
            type: 'datetime',
            labels: {
                overflow: 'justify'
            }
        },
        yAxis: {
            title: {
                text: 'Video RTT (ms)'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ // Light air
                from: 0.0,
                to: 250.0,
                color: '#D3E4BA',
                label: {
                    text: 'Excellent',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Light breeze
                from: 250.0,
                to: 400.0,
                color: '#BBE7DB',
                label: {
                    text: 'Moderate',
                    style: {
                        color: '#09181D'
                    }
                }
            }, { // Gentle breeze
                from: 400.,
                to: 500.,
                color: '#BBE7DB',
                label: {
                    text: 'Moderate',
                    style: {
                        color: '#09181D'
                    }
                }
            },{ // Gentle breeze
                from: 500.,
                to: 20000.,
                color: '#EDD0D9',
                label: {
                    text: 'Poor',
                    style: {
                        color: '#09181D'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' ms'
        },
        plotOptions: {
            spline: {
                lineWidth: 2,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1000, // one hour
                pointStart: Date.now(),
            }
        },
        series: [{
            name: 'Video RTT',
            data: _data

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
}