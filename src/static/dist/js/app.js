window.App = angular.module('precallTestApp', []);
window.App.isWebRTC = false;
window.App.pretestcall = undefined;

window.App.log = function(isFinal, body) {
    body = body || '';
    var isError = !!body.message;

    var body = Array.from(arguments).slice(1).join(' ');
    body = isError ? ('Error: ' + body) : body;

    var now = new Date();

    body = '[' + now.toLocaleTimeString() + '] ' + body;
    var $li = $('<li/>').text(body);

    if (isError) {
        $li.addClass('error');
    } else if (isFinal) {
        $li.addClass('success');
    }

    var proximity = 20;
    var progressBarHeight = $('#progress-wrapper').height();
    var curPos = $('#log-wrapper').scrollTop();
    var paneHeight = $('#log-wrapper').height();
    var listHeight = $('ul#logs').height();

    $('ul#logs').append($li);

    if (curPos + paneHeight > listHeight - progressBarHeight - proximity) {
        $('#log-wrapper').scrollTop($('#log-wrapper')[0].scrollHeight);
    }
};

$(document).ready(function() {
    window.App.pretestcall = new WebRTCPreCallTest();
    window.App.log(false, 'Checking browser details...');

    window.App.pretestcall.browserDetails().then(function(result){
       if (result.supportWebRTC==false){
           window.App.log(false, 'Warning: WebRTC support was not detected. WebRTC tests may be disabled.');
       }else{
           window.App.log(false, 'Browser: '+result.browserType);
           window.App.log(false, 'Browser Version: '+result.version);
           window.App.log(true, 'WebRTC is supported !');
       }
    });
});

