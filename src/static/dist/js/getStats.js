'use strict';

function standardizeReport(response) {
    if(!!navigator.mozGetUserMedia) {
        return response;
    }

    var standardReport = {};
    response.result().forEach(function(report) {
        var standardStats = {
            id: report.id,
            type: report.type,
        };
        report.names().forEach(function(name) {
            standardStats[name] = report.stat(name);
        });
        standardReport[standardStats.id] = standardStats;
    });

    return standardReport;
}

function _getStats(peerConnection, selector, successCb, failureCb) {
    if(!!navigator.mozGetUserMedia) {
        // User is using FireFox
        return peerConnection.getStats(selector, successCb, failureCb);
    } else {
        // User is using Chrome
        return peerConnection.getStats(function(response) {
            var report = standardizeReport(response);
            successCb(report);
        }, selector, failureCb);
    }
}