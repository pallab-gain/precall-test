window.App = window.App || {};
window.App.isRunning = false;

function getDefaultConfiguration(test_name) {
    return [
        {id: test_name + "test-timeout", label: "Test Timeout", value: 30, type: "textbox"},
        {id: test_name + "turn-url", label: "Turn URL", value: "turn:turn.callstats.io:31000", type: "textbox"},
        {id: test_name + "turn-uname", label: "Turn User name", value: "1512655041:applicant", type: "textbox"},
        {id: test_name + "turn-password", label: "Turn Password", value: "1oBHv8fU0LBPVYu6RBOuy5Da7u4=", type: "textbox"},
    ];
}

window.App.suites = [
    {
        name: 'Hardware', tests: [
            {name: 'Audio', description: 'Get list of audio in/out devices', run: audioDeviceList},
            {name: 'Video', description: 'Get list of video devices', run: videoDeviceList},
            {name: 'Video Resolution', description: 'Get supported video resolution', run: resolutionSupport},
        ]
    },
    {
        name: 'Network/Connectivity', tests: [
            {
                name: 'Network',
                description: 'Check Network support details. Whether your setup support, TCP, UDP, IPv6, TLS. Also check if it supports' +
                'Relay, reflexive, and host only communication',
                run: networkTestDetails,
                configOptions: getDefaultConfiguration("Network"),

            },
            {
                name: 'Connectivity Test',
                description: 'Check whether your setup supports Relay, reflexive, and host only communication',
                run: connectivityTestDetails,
                configOptions: getDefaultConfiguration("Connectivity Test"),
            },
        ]
    },
    {
        name: 'Bandwidth', tests: [
            {
                name: 'Data Throughput',
                description: 'Check current data throughput',
                run: dataThroughputTest,
                configOptions: getDefaultConfiguration("Data Throughput"),
            },
            {
                name: 'Audio QoS', description: 'Check current audio QoS',
                run: audioQoSTest,
                configOptions: getDefaultConfiguration("Audio QoS"),
            },
            {
                name: 'Audio-Video QoS',
                description: 'Check current audio-video QoS',
                run: videoQoSTest,
                configOptions: getDefaultConfiguration("Audio-Video QoS"),
            }
        ]
    }];


function audioDeviceList(root, config, reporters, done) {
    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting audio device list. Calling precalltest.hardwareDetails(true)...');
        root.App.pretestcall.hardwareDetails(true).then(function (success) {
            if (success.data.deviceSupport == false) {
                done(new Error('Audio device not supported'));
                return
            }
            if (typeof success.err == false) {
                done(new Error('Error collecting audio device details' + e.message));
                return
            }
            for (item in success.data.deviceList) {
                var data = "Audio Device Info: label - " + success.data.deviceList[item].label + " " +
                    ",\nkind - " + success.data.deviceList[item].kind;
                log(data);
            }

            done('Audio hardware details collected.')
        }).catch(function (e) {
            done(new Error('Could not call precalltest.hardwareDetails(true): ' + e.message));
            return;
        });
    }
}


function videoDeviceList(root, config, reporters, done) {
    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting audio device list. Calling precalltest.hardwareDetails(false)...');
        root.App.pretestcall.hardwareDetails(false).then(function (success) {
            if (success.data.deviceSupport == false) {
                done(new Error('Video device not supported'));
                return
            }
            if (typeof success.err == false) {
                done(new Error('Error collecting video device details' + e.message));
                return
            }
            for (item in success.data.deviceList) {
                var data = "Video Device Info: label - " + success.data.deviceList[item].label + " " +
                    ",\nkind - " + success.data.deviceList[item].kind;
                log(data);
            }
            done('Video hardware details collected.')
        }).catch(function (e) {
            done(new Error('Could not call precalltest.hardwareDetails(false): ' + e.message));
            return;
        });
    }
}

function resolutionSupport(root, config, reporters, done) {
    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting supported video resolution. Calling pretestcall.resolutionSupport(progress)...');
        root.App.pretestcall.hardwareDetails(false).then(function (success) {
            if (success.data.deviceSupport == false) {
                done(new Error('Video device not supported'));
                return
            }
            if (typeof success.err == false) {
                done(new Error('Error collecting video device details' + e.message));
                return
            }

            root.App.pretestcall.resolutionSupport(progress).then(function (resolutions) {
                for (var i in resolutions) {
                    (function (indx) {
                        var item = resolutions[indx];
                        log("Info : " + item.res[0] + "x" + item.res[1] + ", support : " + item.support);
                    })(i)
                }
                done('Video resolution details collected.')
            }, function (e) {
                done(new Error('Could not call pretestcall.resolutionSupport(progress): ' + e.message));
                return;
            });
        }).catch(function (e) {
            done(new Error('Could not call precalltest.hardwareDetails(false): ' + e.message));
            return;
        });
    }
}

function networkTestDetails(root, config, reporters, done) {
    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });

    runTest();

    function runTest() {
        log('Getting current network details. Calling pretestcall.networkTest(progress)...');
        root.App.pretestcall.networkTest(progress, log, config).then(function (result) {
            for (var key in result) {
                (function (curKey) {
                    log("Info : " + curKey + " " + result[curKey].support);
                })(key);
            }
            done('Network test details collected.')
        }).catch(function (e) {
            done(new Error('Could not call pretestcall.networkTest(progress): ' + e.message));
            return;
        });
    }
}

function connectivityTestDetails(root, config, reporters, done) {
    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting current connectivity details. Calling connectivity.checkConnectivity(progress)...');
        root.App.pretestcall.connectivityTest(progress, log, config).then(function (result) {
            for (var key in result) {
                (function (curKey) {
                    log("Info : " + curKey + " " + result[curKey].support);
                })(key);
            }
            done('Connectivity test details collected.')
        }).catch(function (e) {
            done(new Error('Could not call connectivity.checkConnectivity(progress): ' + e.message));
            return;
        });
    }
}

function dataThroughputTest(root, config, reporters, done) {
    $('#' + "audio-container-bandwidth").css('display', 'none');
    $('#' + "audio-container-rtt").css('display', 'none');


    $('#' + "av-container-bandwidth").css('display', 'none');
    $('#' + "av-container-rtt").css('display', 'none');
    $('#' + "data-container").css('display', 'block');

    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting current data throughput. Calling pretestcall.dataThroughputTest(progress,log)...');
        root.App.pretestcall.dataThroughputTest(progress, log, config).then(function (result) {
            log("Info : Total byte send " + result.sendByte);
            log("Info : Total byte receive " + result.recvByte);
            log("Info : Mean bitrate " + parseFloat(result.meanbitrate).toFixed(2) + " kbps");

            dataBandwidthChart(result.bitrates);
            done('Connectivity test details collected.')
        }).catch(function (e) {
            done(new Error('Could not call connectivity.checkConnectivity(progress): ' + e.message));
            return;
        });
    }
}

function audioQoSTest(root, config, reporters, done) {
    $('#' + "audio-container-bandwidth").css('display', 'block');
    $('#' + "audio-container-rtt").css('display', 'block');


    $('#' + "av-container-bandwidth").css('display', 'none');
    $('#' + "av-container-rtt").css('display', 'none');
    $('#' + "data-container").css('display', 'none');



    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });


    runTest();

    function runTest() {
        log('Getting audio QoS. Calling pretestcall.audioOnlyQoS(progress,log)...');
        root.App.pretestcall.audioOnlyQoS(progress, log, config).then(function (result) {
            log("Info : Audio Bandwidth " + result.bandWidthInkbps + " kbps");
            log("Info : Average RTT " + result.data.avgRTT);
            log("Info : Max RTT " + result.data.maxRTT);
            log("Info : Packet Loss " + result.data.packetLoss + " %");

            if (result.bandWidthInkbps > 30 && result.data.packetLoss < 0.5) {
                log("Audio Call Quality : Excellent");
            } else if (result.bandWidthInkbps > 25 && result.data.packetLoss < 5.0) {
                log("Audio Call Quality : Acceptable");
            } else {
                log("Audio Call Quality: Poor")
            }

            if (result.data.avgRTT >= 1000 ) {
                log("Warning : Very high audio RTT. Experience will be very BAD.")
            } else if (result.data.avgRTT >= 800) {
                log("Warning : High audio RTT. Experience will be poor.")
            } else if (result.data.avgRTT >= 600) {
                log("Warning : Considerably high audio RTT. Fair RTT value, but still audio experience may not be smooth.")
            } else if (result.data.avgRTT >= 400) {
                log("Warning : Accepted audio RTT. Good RTT value, audio experience be smooth.")
            } else if(result.data.avgRTT > 0 && result.data.avgRTT < 400 ){
                log("Info : Excellent audio RTT. Audio experience will be very smooth.")
            }

            audioBandwidthChart(result.data.bandwidth_ar);
            audioRTTChart(result.data.rtt_ar);

            done('Audio QoS test details collected.');
        }).catch(function (e) {
            done(new Error('Could not call pretestcall.audioOnlyQoS(progress,log): ' + e.message));
            return;
        });
    }
}

function videoQoSTest(root, config, reporters, done) {

    $('#' + "audio-container-bandwidth").css('display', 'none');
    $('#' + "audio-container-rtt").css('display', 'none');


    $('#' + "av-container-bandwidth").css('display', 'block');
    $('#' + "av-container-rtt").css('display', 'block');
    $('#' + "data-container").css('display', 'none');


    var progress = reporters.progress;
    var log = reporters.log;
    window.App.pretestcall.browserDetails().then(function (result) {
        if (result.supportWebRTC == false) {
            done(new Error('WebRTC is currently only supported by Chrome and Firefox'));
            return;
        }
    });
    runTest();

    function runTest() {
        log('Getting AV QoS. Calling pretestcall.videoOnlyQoS(progress,log)...');
        root.App.pretestcall.hardwareDetails(false).then(function (success) {
            if (success.data.deviceSupport == false) {
                done(new Error('Video device not supported'));
                return
            }
            if (typeof success.err == false) {
                done(new Error('Could not call pretestcall.videoOnlyQoS(progress,log): ' + e.message));
                return
            }

            root.App.pretestcall.videoOnlyQoS(progress, log, config).then(function (result) {

                log("Info : Average Audio RTT " + result.audio.data.avgRTT);
                log("Info : Max Audio RTT " + result.audio.data.maxRTT);
                log("Info : Audio Packet Loss " + result.audio.data.packetLoss + " %");


                log("Info : Average Video RTT " + result.video.data.avgRTT);
                log("Info : Max Video RTT " + result.video.data.maxRTT);
                log("Info : Video Packet Loss " + result.video.data.packetLoss + " %");

                log("Info : Bandwidth " + result.bandWidthInkbps + " kbps");

                if (result.resolution.width == 1280 && result.resolution.height == 720 && result.bandWidthInkbps > 1000 && result.video.data.packetLoss < 0.5) {
                    log("AV Call Quality : Excellent")
                } else if (result.resolution.width == 640 && result.resolution.height == 480 && result.bandWidthInkbps > 600 && result.video.data.packetLoss < 0.5) {
                    log("AV Call Quality : Excellent")
                } else if (result.resolution.width == 352 && result.resolution.height == 288 && result.bandWidthInkbps > 300 && result.video.data.packetLoss < 0.5) {
                    log("AV Call Quality : Excellent")
                } else if (result.resolution.width == 320 && result.resolution.height == 240 && result.bandWidthInkbps > 300 && result.video.data.packetLoss < 0.5) {
                    log("AV Call Quality : Excellent")
                } else if (result.resolution.width == 1280 && result.resolution.height == 720 && result.bandWidthInkbps > 350 && result.video.data.packetLoss < 3) {
                    log("AV Call Quality : Acceptable")
                } else if (result.resolution.width == 640 && result.resolution.height == 480 && result.bandWidthInkbps > 250 && result.video.data.packetLoss < 3) {
                    log("AV Call Quality : Acceptable")
                } else if (result.resolution.width == 352 && result.resolution.height == 288 && result.bandWidthInkbps > 150 && result.video.data.packetLoss < 3) {
                    log("AV Call Quality : Acceptable")
                } else if (result.resolution.width == 320 && result.resolution.height == 240 && result.bandWidthInkbps > 150 && result.video.data.packetLoss < 3) {
                    log("AV Call Quality : Acceptable")
                } else {
                    log("AV Call Quality : Poor")
                }

                console.log(result);
                if (result.audio.data.avgRTT >= 1000 ) {
                    log("Warning : Very high audio RTT. Experience will be very BAD.")
                } else if (result.audio.data.avgRTT >= 800) {
                    log("Warning : High audio RTT. Experience will be poor.")
                } else if (result.audio.data.avgRTT >= 600) {
                    log("Warning : Considerably high audio RTT. Fair RTT value, but still audio experience may not be smooth.")
                } else if (result.audio.data.avgRTT >= 400) {
                    log("Warning : Accepted audio RTT. Good RTT value, audio experience be smooth.")
                } else if(result.audio.data.avgRTT > 0 && result.audio.data.avgRTT < 400 ){
                    log("Info : Excellent audio RTT. Audio experience will be very smooth.")
                }

                if (result.video.data.avgRTT >= 1000 ) {
                    log("Warning : Very high video RTT. Experience will be very BAD.")
                } else if (result.video.data.avgRTT >= 800) {
                    log("Warning : High video RTT. Experience will be poor. There will be packet loss, video frame freeze issue.")
                } else if (result.video.data.avgRTT >= 600) {
                    log("Warning : Considerably high video RTT. Fair RTT value, but still you may experience periodic video frame lose.")
                } else if (result.video.data.avgRTT >= 400) {
                    log("Warning : Accepted video RTT. Good RTT value, video experience be smooth.")
                } else if(result.video.data.avgRTT > 0 && result.video.data.avgRTT < 400 ){
                    log("Info : Excellent video RTT. Video experience will be very smooth.")
                }

                avBandwidthChart(result.video.data.bandwidth_ar);
                avRTTChart(result.video.data.rtt_ar);

                done('AV QoS test details collected.')
            }).catch(function (e) {
                done(new Error('Could not call pretestcall.videoOnlyQoS(progress,log): ' + e.message));
                return;
            });

        }).catch(function (e) {
            done(new Error('Could not call pretestcall.videoOnlyQoS(progress,log): ' + e.message));
            return;
        });
    }
}
