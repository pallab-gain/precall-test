package precalltest

import (
	"html/template"
	"net/http"

	"log"
)

var tpl *template.Template

func init(){
	var err error
	tpl, err = template.New("").Delims("[[","]]").ParseGlob("templates/*.html")
	if err !=nil{
		log.Fatal(err)
	}
	http.HandleFunc("/", indexHandler)
	http.Handle("/favicon.ico", http.NotFoundHandler())
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	err := tpl.ExecuteTemplate(w, "index.html", nil)
	if err != nil {
		log.Fatalln(err)
	}
}