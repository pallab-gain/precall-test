'use strict';

import {HTTPClient} from "../lib/httpclient/httpclient";

/**
 * @class Verify is network is available.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class AvailabilityCheck {
    /** @constructs
     * @param {Object} configs - Test Duration. Number of time in second the test will run
     */
    constructor(configs) {
        let self = this;
        self.testDuration = parseFloat( configs['test-timeout'] );
        self.timeout_id = undefined;
    }
    /**
    * @returns {Promise} Promise object that contains whether network is available, no-data, or unavailable.
    */
    isNetworkAvailable(){
        let self = this;
        let isOnline = false;
        if (navigator && navigator.onLine) {
            isOnline = navigator.onLine;
        }
        if(typeof self.timeout_id!='undefined'){
            clearTimeout(self.timeout_id);
            self.timeout_id = undefined;
        }


        return new Promise( (resolve,reject) => {
            self.timeout_id=setTimeout( function(){
                resolve(false)
            }, self.testDuration*1000);

            if ( isOnline != true ){
                resolve(true)
            }else{
                // const generate_204 = 'http://clients3.google.com/generate_204';
                const generate_204 = 'https://api.github.com/users/octocat/orgs';
                const httpclick = new HTTPClient();
                httpclick.network_test(generate_204,5*1000).then(function(success){
                    resolve(true)
                }).catch(function(err){
                    reject(err)
                })
            }
        });
    }
}

export {AvailabilityCheck};