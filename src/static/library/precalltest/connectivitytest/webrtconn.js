'use strict';
import {Utils} from "../lib/utils";

/**
 * @class Core WebRTC connection class.
 * Establish a p2p connection between two peer with provided connection type.( loop-back connection )
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class WebRTCCon {
    /** @constructs
     * @param {Object} configs - Test Duration, Turn details
     *
     */
    constructor(configs) {
        let self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;
        self.dc1 = undefined;
        self.canExchangeData = false;
        self.config = {'iceServers': []};
        self.util = new Utils();
        self.testDuration = parseFloat( configs['test-timeout'] );
        self.turnDetails = {
            urls:  configs['turn-url'],
            username:  configs['turn-uname'],
            credential:  configs['turn-password'],
        };
        self.timeout_id = undefined;
        self.callback = {
            onSuccess : undefined,
        };
    }
    /**
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */
    _getRemoteTurn(isUDP) {
        let self = this;
        let iceServer = _.clone(self.turnDetails,true);
        if (isUDP) {
            iceServer.urls = (iceServer.urls+"?transport=udp")
        } else {
            iceServer.urls = (iceServer.urls+"?transport=tcp")
        }
        self.config.iceServers.push(iceServer);
        return self.config;
    }
    /**
     * @param {Object} iceConfig - Turn ICE configuration
     * @returns {Object} PeerConnection object
     */
    _createPC(iceConfig) {
        let self = this;
        return new RTCPeerConnection(iceConfig, null);
    }

    /**
     * @param {Object} peerConnection - PeerConnection object
     * @returns {Object} DataChannel with associated peer connection
     */
    _createDC(peerConnection) {
        let self = this;
        return peerConnection.createDataChannel(null);
    }

    /**
     * Close peer connection, and free resources.
     *
     */
    _closePC() {
        let self = this;
        self.pc1.close();
        self.pc2.close();
        self.pc1 = null;
        self.pc2 = null;
    }

    /**
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @param {Function} isGood - Supported Relay candidate type
     */
    _registerIceEvenets(pc1, pc2, isGood) {
        isGood = isGood || function(){};
        let self = this;
        pc1.addEventListener('icecandidate', function (e) {
            if (e.candidate) {
                let parsed = self.util.parseCandidate(e.candidate.candidate);
                if (isGood(parsed)) {
                    pc2.addIceCandidate(e.candidate);
                }
            }
        });
    }
    /**
     * @param {Object} dc - DataChannel the loopback session
     * @param {Object} pc - Associated peer connection with given data channel
     */
    _registerDCEvenets(dc, pc) {
        let self = this;

        const data = 'hello from peer remote peer';
        dc.addEventListener('open', function (e) {
            dc.send(data);
        });

        dc.addEventListener('message', function (e) {
            if (e.data !== data) {
                console.error('Invalid data transmitted.');
            } else {
                self.canExchangeData = true;
                console.log('can exchange data ', true);
                self.callback.onSuccess(self.canExchangeData);
            }

        });

        pc.addEventListener('datachannel', function (e) {
            let remoteDC = e.channel;
            remoteDC.addEventListener('message', function (evt) {
                if (evt.data !== data) {
                    console.error('Invalid data transmitted.');
                } else {
                    remoteDC.send(data);
                }
            });
        });
    }

    /**
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @returns {Promise} A promise object that represents boolean true or false
     */
    _mayBeStartStreaming(pc1, pc2) {
        let self = this;
        self.canExchangeData = false;
        console.log('test duration ',self.testDuration);
        return new Promise((resolve, reject) => {
            let timeoutID = setTimeout(function(noop){
                console.log('end test from timeout ');
                resolve(self.canExchangeData)
            }, self.testDuration*1000);

            pc1.createOffer().then(function (offer) {
                pc1.setLocalDescription(offer);
                pc2.setRemoteDescription(offer);
                pc2.createAnswer().then(function (answer) {
                    pc2.setLocalDescription(answer);
                    pc1.setRemoteDescription(answer);
                });
                self.callback.onSuccess = function(success){
                    clearTimeout(timeoutID);
                    console.log('end test from callback');
                    resolve(success)
                }
            }).catch(function (err) {
                if(typeof timeoutID !='undefined' ){
                    clearTimeout(timeoutID);
                }
                reject(err);
            });
        });

    }
    /**
     * @param {Function} isGood - Supported Relay candidate type
     * @returns {Promise} A promise object that represents boolean true or false
     */
    testConnectivity(isGood) {
        let self = this;

        const iceConfig = self._getRemoteTurn(true);
        self.pc1 = self._createPC(iceConfig);
        self.pc2 = self._createPC(iceConfig);

        self._registerIceEvenets(self.pc1, self.pc2, isGood);
        self._registerIceEvenets(self.pc2, self.pc1, isGood);

        self.dc1 = self._createDC(self.pc1);
        self._registerDCEvenets(self.dc1, self.pc2);

        return new Promise((resolve,reject)=>{
            self._mayBeStartStreaming(self.pc1, self.pc2).then(function(success){
                self._closePC();
                resolve(success)
            }).catch(function(err){
                self._closePC();
                reject(err);
            })
        });

    }
}

export {WebRTCCon};
