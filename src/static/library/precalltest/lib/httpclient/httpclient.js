'use strict';

/**
 * @class HTTPClient wrapper.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class HTTPClient {
    /** @constructs */
    constructor() {
        let self = this;
    }

    /**
     * Provide a 12 digit random string
     * @returns {string} 12 digit long random string
     */
    _get_transaction_id() {
        return Math.random().toString(14).substr(2, 12)
    };

    /**
     * POST call wrapper.
     * @param {number} time - Current time.
     * @param {number} datapoint - current value
     * @returns {Promise} Promise object that contains jquery response object.
     */
    do_post(url, timeOut, data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": 'POST',
                "timeout": timeOut,
                "headers": {
                    "content-type": "application/json",
                },
                "processData": false,
                "data": data,
                "success": function (response) {
                    resolve(response)
                },
                "error": function (xhr, status, error) {
                    console.log(xhr, status, error);
                    reject(error)
                }
            });
        });
    }

    /**
     * Do a simple GET request to check whether network is available.
     * @param {string} url - URL of network request
     * @param {number} timeout - Timeout of current ajax request
     * @returns {Promise} Promise object that contains jquery response object.
     */
    network_test(url, timeOut) {
        return new Promise((resolve, reject) => {
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": "GET",
                "timeout": timeOut,
                "success": function (response) {
                    resolve(response)
                },
                "error": function (xhr, status, error) {
                    reject(xhr);
                }
            });
        });
    }

}

export {HTTPClient};