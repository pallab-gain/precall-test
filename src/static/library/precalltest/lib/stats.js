'use strict';


/**
 * @class Stats class that collect rtt, bwe values and
 * provides aggregated value
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class StatisticsAggregate{
    /** @constructs
     * @param {number} rampUpThreshold - Default ramp up threshold value for the given test.
     */
    constructor(rampUpThreshold){
        let self = this;
        self.startTime_ = 0;
        self.sum_ = 0;
        self.count_ = 0;
        self.max_ = 0;
        self.rampUpThreshold_ = rampUpThreshold;
        self.rampUpTime_ = Infinity;
    }
    /**
     * Check connectivity detail
     * @param {number} time - Current time.
     * @param {number} datapoint - current value
     */
    add(time, datapoint) {
        let self = this;
        if (self.startTime_ === 0) {
            self.startTime_ = time;
        }
        this.sum_ += datapoint;
        self.max_ = Math.max(self.max_, datapoint);
        if (self.rampUpTime_ === Infinity &&
            datapoint > self.rampUpThreshold_) {
            self.rampUpTime_ = time;
        }
        self.count_++;
    }
    /**
     * Get average value
     * @returns {number} Average value
     */
    getAverage() {
        let self = this;
        if (self.count_ === 0) {
            return 0;
        }
        return Math.round(self.sum_ / self.count_);
    }
    /**
     * Get Maximum value
     * @returns {number} Maximum value
     */
    getMax() {
        let self = this;
        return self.max_;
    }
    /**
     * Get raqmp up time value
     * @returns {number} RampUp time.
     */
    getRampUpTime() {
        let self = this;
        return Math.round(self.rampUpTime_ - self.startTime_);
    }
}

export {StatisticsAggregate};