#!/usr/bin/env bash

# Helper script to
# 1. Generate SDK and resolve JS version compatibility
# 2. Generate JSDoc

OUT_DIR=`pwd`
function generateSDK(){
    fromjs=${OUT_DIR}/library/precalltest/webrtcstat-library.js
    tojs=${OUT_DIR}/dist/js/webrtcstat-library.js

    browserify ${fromjs} --standalone  WebRTCPreCallTest -t babelify --outfile ${tojs}
}

function generateJSDoc(){
    dir=${OUT_DIR}/../../
    ${dir}/node_modules/.bin/jsdoc ${OUT_DIR}/library/precalltest -R ${OUT_DIR}/readme.md -r -d ${dir}/docs
}

$1