'use strict';
import {Utils} from "../lib/utils";
import {StatisticsAggregate} from "../lib/stats";

/**
 * @class Provides Google Chrome specific Audio only call quality matrices.
 * Try to create a Audio only loop-back connection using relay-only connection type.
 * Run the test for certain period of time, and calculate audio bandwidth, RTT, packet
 * loss of the connection to gather audio only quality webRTC call quality matrices.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
class ChromeAudioQoS {
    /** @constructs
     *  @param {Object} configs - Test Duration, Turn details
     */
    constructor(configs) {
        let self = this;
        self.pc1 = undefined;
        self.pc2 = undefined;

        self.config = {'iceServers': []};
        self.util = new Utils();
        self.runtest_id = undefined;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],

            },
            'stats': undefined,
        };
        self.progress = undefined;
        self.log = undefined;
        self.ITR = 0;
        self.UPTO = parseFloat( configs['test-timeout'] );
        self.turnDetails = {
            urls:  configs['turn-url'],
            username:  configs['turn-uname'],
            credential:  configs['turn-password'],
        };
    }
    /**
     * Generate ICE candidate
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */
    _getRemoteTurn(isUDP) {
        let self = this;
        let iceServer = _.clone(self.turnDetails,true);
        if (isUDP) {
            iceServer.urls = (iceServer.urls+"?transport=udp")
        } else {
            iceServer.urls = (iceServer.urls+"?transport=tcp")
        }
        self.config.iceServers.push(iceServer);
        return self.config;
    }

    /**
     * Create peer connection object
     * @param {Object} iceConfig - Turn ICE configuration
     * @returns {Object} PeerConnection object
     */
    _createPC(iceConfig) {
        let self = this;
        return new RTCPeerConnection(iceConfig, null);
    }

    /**
     * Close peer connection, and free resources.
     *
     */
    _closePC() {
        console.log('closing peer connection');
        let self = this;
        if (typeof self.runtest_id != 'undefined') {
            clearTimeout(self.runtest_id);
            self.runtest_id = undefined;
        }

        if (typeof self.pc1 != 'undefined') {
            self.pc1.getLocalStreams()[0].getTracks().forEach(function (track) {
                track.stop();
            });
            self.pc1.close();
            self.pc1 = undefined;
        }
        if (typeof self.pc2 != 'undefined') {
            self.pc2.close();
            self.pc2 = undefined;
        }

    }
    /**
     * Collect webRTC matrices for audio only loop-back call.
     * @param {Object} result - webRTC stats object
     * @param {Object} isLocal - Whether this stats is for local peer, or remote peer.
     * @param {Object} self - Scope object of callee function
     */
    _goStats(result, isLocal, self) {
        if (isLocal == true) {
            self.rtcstat.stats = result;
        }

        function __collect(obj, item) {
            var isSending = item.id.indexOf('_send') !== -1; // sender or receiver
            const sendBandWidth = (0.008 * self.rtcstat.stats.bandwidth.speed);
            if (isSending == true) {
                if (isLocal == true && typeof self.pc1 != 'undefined' && sendBandWidth > 0 ) {
                    self.ITR += 1;
                    self.progress(Math.round(100. * self.ITR) / self.UPTO);
                    self.log('Info : current audio transmitting rate ' + sendBandWidth.toFixed(2) + " kbps");
                    obj.bandwidth_ar.push( parseFloat(sendBandWidth.toFixed(2)) );
                }
                if (typeof item.googRtt != 'undefined') {
                    obj.rtt.add(item.timestamp, parseFloat(item.googRtt));
                    obj.rtt_ar.push( parseFloat(item.googRtt) );
                }
                if (typeof item.packetsSent != 'undefined') {
                    obj.packetsent = item.packetsSent;
                }
            } else {
                if (typeof item.packetsLost != 'undefined') {
                    obj.packetloss = item.packetsLost;
                }
            }
        }
        if(typeof result.results != 'undefined') {
            result.results.forEach(function (item) {
                if (item.type === 'ssrc' && item.transportId === 'Channel-audio-1') {
                    const isAudio = item.mediaType === 'audio'; // audio or video
                    __collect(isAudio == true ? self.rtcstat.audio : self.rtcstat.video, item);
                }
            });
        }else{
            console.log('what -> ',result);
        }
        if (isLocal == true && typeof self.pc1 == 'undefined') {
            result.nomore();
        }
        if (isLocal == false && typeof self.pc2 == 'undefined') {
            result.nomore();
        }
    }
    /**
     * Register ICE events
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @param {Function} isGood - Supported Relay candidate type
     */
    _registerIceEvenets(pc1, pc2, isGood) {
        let self = this;
        pc1.addEventListener('icecandidate', function (e) {
            if (e.candidate) {
                let parsed = self.util.parseCandidate(e.candidate.candidate);
                if (isGood(parsed)) {
                    pc2.addIceCandidate(e.candidate);
                }
            }
        });
        pc2.addEventListener('addstream', function (e) {
           getStats(self.pc1, function (result) {
                self._goStats(result, true, self);
            }, 1 * 1000);
            getStats(self.pc2, function (result) {
                self._goStats(result, false, self);
            }, 1 * 1000);
        });

    }
    /**
     * Collect, finalize audio only loopback call stat after completing the full session.
     *
     * @returns {Object} Audio only call quality control matrices
     */
    _collect_stat() {
        let self = this;
        function __getstat(data) {
            const avgRTT = data.rtt.getAverage();
            const maxRTT = data.rtt.getMax();
            const packetLossPercentage = ((100.0 * data.packetloss) / data.packetsent);
            return {
                avgRTT: (avgRTT).toFixed(2),
                maxRTT: (maxRTT).toFixed(2),
                packetLoss: (packetLossPercentage).toFixed(2),
                rtt_ar : data.rtt_ar,
                bandwidth_ar : data.bandwidth_ar,
            }
        }

        const sendBandWidth = (0.008 * _.get(self.rtcstat,"stats.bandwidth.speed",0));
        return {
            data: __getstat(self.rtcstat.audio),
            bandWidthInkbps: (sendBandWidth).toFixed(2),
        };
    }

    /**
     * Try to create a loopback peer connection using relay server
     * @param {Object} pc1 - First peer connection in the loopback session
     * @param {Object} pc2 - Second peer connection in the loopback session
     * @returns {Promise} A promise object that represents webrtc audio only call quality stats objects
     */
    _mayBeStartStreaming(pc1, pc2) {
        let self = this;
        self.rtcstat = {
            'audio': {
                'packetsent': -1,
                'packetloss': -1,
                'rtt': new StatisticsAggregate(),
                'bandwidth_ar': [],
                'rtt_ar': [],
            },
            'stats': undefined,
        };
        self.ITR = 0;
        return new Promise((resolve, reject) => {
            if (typeof self.runtest_id != 'undefined') {
                clearTimeout(self.runtest_id);
                self.runtest_id = undefined;
            }

            self.runtest_id = setTimeout(function () {
                const audioQoS = self._collect_stat.apply(self);
                resolve(audioQoS);
            }, self.UPTO * 1000);


            pc1.createOffer().then(function (offer) {
                pc1.setLocalDescription(offer);
                pc2.setRemoteDescription(offer);
                pc2.createAnswer().then(function (answer) {
                    pc2.setLocalDescription(answer);
                    pc1.setRemoteDescription(answer);
                });
            }).catch(function (err) {
                reject(err);
            });
        });

    }
    /**
     * Provides audio only call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
     */
    testQoS(progress, log) {
        progress = progress||function(){};
        log = log||function(){};

        let self = this;
        self.progress = progress;
        self.log = log;

        let iceConfig = self._getRemoteTurn(true);
        self.pc1 = self._createPC(iceConfig);
        self.pc2 = self._createPC(iceConfig);

        self._registerIceEvenets(self.pc1, self.pc2, self.util.isRelay);
        self._registerIceEvenets(self.pc2, self.pc1, self.util.isRelay);

        const constraints = {
            audio: true,
            video: false,
        };

        return new Promise((resolve, reject) => {
            //get user media
            self.util.doGetUserMedia(constraints, function (stream) {
                self.pc1.addStream(stream);
                self._mayBeStartStreaming(self.pc1, self.pc2).then(function (success) {
                    self._closePC();
                    resolve(success)
                }).catch(function (err) {
                    self._closePC();
                    reject(err);
                });
            }, function (err) {
                reject(err);
            });
        });

    }
}

export {ChromeAudioQoS};
