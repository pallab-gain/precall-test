'use strict';
import {AvailabilityCheck} from '../precalltest/networktest/availabilitycheck'
import {Browser} from '../precalltest/browsertest/browser'
import {Hardware} from '../precalltest/hardwaretest/hardware'
import {Network} from './networktest/networkcheck'
import {Connectivity} from './connectivitytest/connectivitytest'
import {DataThroughput} from "./throughputtest/datathroughtputtest";
import {ChromeAudioQoS} from "./throughputtest/chromeaudioqos";
import {MozAudioQoS} from "./throughputtest/mozaudioqos";
import {ChromeVideoQoS} from "./throughputtest/chromevideoqos";
import {MozVideoQoS} from "./throughputtest/mozvideoqos";

/**
 * @class Principle SDK wrapper class.
 * Client will only have access to this class, and communicate internal classes through it.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class WebRTCPreCallTest {
    /** @constructs
     */
    constructor() {
        let self = this;
    }

    /**
     * Provides online/offline status
     * @returns {bool} Verify if you are online, or offline
     */
    isOnline() {
        let self = this;
        const network = new AvailabilityCheck();
        return network.isNetworkAvailable();
    }

    /**
     * Get the current browser details
     * @returns {Object} represents browserType, version, and supportWebRTC details of the browser
     */
    browserDetails() {
        const browser = new Browser();
        return new Promise((resolve, reject) => {
            resolve(browser.getDetails())
        });
    }

    /**
     * Returns list of available audio or video input devices
     * @param {bool} isAudio - Check if you need audio, or video hardware details
     * @returns {Promise} Promise object represents list of available audio or video input devices
     */
    hardwareDetails(isAudio) {
        const hardware = new Hardware();
        return hardware.supportDetails(isAudio);
    }

    /**
     * Returns list of supported video resolution
     * @param {function} progress binder to show progress in UI
     * @param {function} log binder to log in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} Promise object represents list of supported video resolution.
     */
    resolutionSupport(progress, log, config) {
        const hardware = new Hardware(config);
        return hardware.resolutionTest(progress);
    }

    /**
     * Get network connectivity details
     * @param {function} progress binder to show progress in UI
     * @param {function} log binder to log in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
     */
    networkTest(progress, log, config) {
        const networktest = new Network(config);
        return networktest.getNetworkDetail(progress, log);
    }

    /**
     * Check connectivity detail
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} Promise object that contains Relay, Reflexive, Host only connection support details.
     */
    connectivityTest(progress, log, config) {
        const connectivity = new Connectivity(config);
        return connectivity.checkConnectivity(progress, log);
    }

    /**
     * Provides Data Throughput stats
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc data throughtput stats objects
     */
    dataThroughputTest(progress, log, config) {
        const throughput = new DataThroughput(config);
        return throughput.dataThroughputTest(progress, log)
    }

    /**
     * Provides chrome audio only call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
     */
    chromeAudioOnlyQoS(progress, log, config) {
        const audioQoS = new ChromeAudioQoS(config);
        return audioQoS.testQoS(progress, log);
    }

    /**
     * Provides mozilla audio only call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
     */
    mozAudioOnlyQoS(progress, log, config) {
        const audioQoS = new MozAudioQoS(config);
        return audioQoS.testQoS(progress, log);
    }

    /**
     * Provides specific audio only call quality matrices. Currently only support mozilla firefox, and google chrome
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio only call quality matrices
     */
    audioOnlyQoS(progress, log, config) {
        let self = this;
        if (adapter.browserDetails.browser == "firefox") {
            return self.mozAudioOnlyQoS(progress, log, config);
        } else {
            return self.chromeAudioOnlyQoS(progress, log, config);
        }
    }

    /**
     * Provides Chrome specific Audio Video call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
     */
    chromeVideoOnlyQoS(progress, log, config) {
        const videoQoS = new ChromeVideoQoS(config);
        return videoQoS.testQoS(progress, log);
    }

    /**
     * Provides Firefox specific Audio Video call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
     */
    mozVideoOnlyQoS(progress, log, config) {
        const videoQoS = new MozVideoQoS(config);
        return videoQoS.testQoS(progress, log);
    }

    /**
     * Provides Audio Video call quality matrices
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @param {Object} configs - Test Duration, Turn details
     * @returns {Promise} A promise object that represents webrtc audio video call quality matrices
     */
    videoOnlyQoS(progress, log, config) {
        let self = this;
        if (adapter.browserDetails.browser == "firefox") {
            return self.mozVideoOnlyQoS(progress, log, config);
        } else {
            return self.chromeVideoOnlyQoS(progress, log, config);
        }
    }
}

module.exports = WebRTCPreCallTest;