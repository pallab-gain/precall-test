## Precall Test Application ##

[This application](https://bitbucket.org/pallab-gain/precall-test) provides a set of tests to understand internet connection
better before joining a WebRTC call. When joining a call based on webRTC, the user does not know
beforehand how good or bad the call quality will be. For example, if their internet connection is really bad and if the
user knows it before the call, they might opt to join the call without any video, and just enabling audio. This pre-call
test include investigating the capabilities of the internet connection, hardware support details, network health, and other webRTC related
matrices.

## Supported Browser ##
- [x] Google Chrome
- [x] Firefox
- [ ] Opera
- [ ] IE
- [ ] Edge

## Supported Test ##
- Hardware
    - Audio
        - Get list of supported audio hardware
    - Video
        - Get list of supported video hardware
    - Video Resolution
        - Check supported WxH resolution

- Network and Connectivity
    - UDP/TCP/TLS
        - Test if UDP,TCP,TLS connection can be established with relay server.
    - IPv6
        - Test if IPv6 candidates can be gathered
    - Relay
        - Test if connections can be established between peers through Relay server
    - Reflexive
        - Test if connections can be established between peers through NAT
    - Host
        - Test if connections can be established between peers with sample IP address

- Bandwidth
    - Data Throughput
        - Check data throughput of your client by establishing a loop-back call
    - Audio QoS
        - Check audio only call quality your client by establishing a loop-back call
    - AV QoS
        - Check AV call quality your client by establishing a loop-back call

## How does the application works ##
1. Hardware
    1. [Audio](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-50)
        1. Call  `navigator.mediaDevices.enumerateDevices()` to gather list of devices, and return all device that matches `{'kind': 'audioinput'}`
    2. [Video](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-73)
        1. Call  `navigator.mediaDevices.enumerateDevices()` to gather list of devices, and return all device that matches `{'kind': 'videoinput'}`
    3. [Video Resolution](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-95)
        1. Run a test against [pre defined list of resolutions](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-6), and check if current client
        support them using [navigator.mediaDevices.getUserMedia(constraints)](/src/static/library/precalltest/hardwaretest/hardware.js?#hardware.js-110) method.

2. Network and Connectivity
    1. [UDP/TCP/TLS/IPv6](/src/static/library/precalltest/networktest/networkcheck.js)
        1. Try to create a peer connection using relay only server(s), and analyze the gathered ice candidates.
        2. Verify supported connection types using [utils.js](/src/static/library/precalltest/lib/utils.js?#utils.js-45).
    2. [Relay/Reflexive/Host](/src/static/library/precalltest/connectivitytest/connectivitytest.js)
        1. Create a [Datachannel only](/src/static/library/precalltest/connectivitytest/webrtconn.js) loop-back peer connection using specific type.
            1. Relay only
            2. Reflexive
            3. Host only
        2. Check if connections can be established between peers, and data can be exchanges.

3. Bandwidth
    1. Data Throughput
        1. Try to create a [Datachannel only](/src/static/library/precalltest/throughputtest/datathroughtputtest.js) loop-back connection using relay-only connection type.
        2. Exchange data between two peers for certain period of time, and calculate data transmission rate, packet loss
        percentage to get data throughput matrices.
    2. Audio QoS
        1. Try to create a [audio only](/src/static/library/precalltest/throughputtest/audioqos.js) loop-back connection using relay-only connection type.
        2. Run the test for certain period of time, and calculate audio bandwidth, RTT, packet loss of the connection to gather audio only
        quality webRTC call quality matrices.
    3. Audio-Video QoS
        1. Try to create a [Audio+Video](/src/static/library/precalltest/throughputtest/videoqos.js) loop-back connection using relay-only connection type.
        2. Run the test for certain period of time, and calculate AV bandwidth, RTT, packet loss of the connection to gather audio only
        quality webRTC call quality matrices.
