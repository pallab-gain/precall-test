'use strict';

/**
 * @class Utility class specific to this pre-call test.
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */
class Utils {
    /** @constructs */
    constructor() {
        let self = this;
    }
    /**
     * Return transport type
     * @param {protocol} protocol - WebRTC protocol type
     * @param {transport} tranport - Transport candidate
     * @returns {string} Transport type. TLS, TCP, or UDP
     */
    _transportType(protocol, tranport) {
        let self = this;
        var b = "NONE", c = tranport >> 24;
        if ("rtp" === protocol && c >= 0 && c <= 2) {
            switch (c) {
                case 0:
                    b = "TLS";
                    break;
                case 1:
                    b = "TCP";
                    break;
                case 2:
                    b = "UDP"
            }
        }
        return b
    }
    /**
     * Return object representation of parsed ice candidate
     * @param {string} text - WebRTC ICE candidate
     * @returns {Object} JSON object representation of ICE candidate.
     */
    parseCandidate(text) {
        let self = this;

        var candidateStr = 'candidate:';
        var pos = text.indexOf(candidateStr) + candidateStr.length;
        var fields = text.substr(pos).split(' ');


        return {
            'protocol': "1" === fields[1] ? "rtp" : "rtcp",
            'transport': fields[2],
            'typeTransport': self._transportType("1" === fields[1] ? "rtp" : "rtcp", fields[3]),
            'ipv6': -1 !== fields[4].indexOf(":"),
            'ipAddress': fields[4],
            'port': fields[5],
            'type': fields[7],
        }
    };
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support UDP
     */
    isUDP(e) {
        return e.typeTransport === 'UDP'
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support TCP
     */
    isTCP(e) {
        return e.typeTransport === 'TCP'
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support TLS
     */
    isTLS(e) {
        return e.typeTransport === 'TLS'
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support IPv6
     */
    isIPv6(e) {
        return e.ipv6
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support Relay
     */
    isRelay(e) {
        return e.type === "relay";
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support non Host
     */
    isNotHostCandidate(e) {
        return e.type === "host";
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support srflx
     */
    isReflexive(e) {
        return e.type === "srflx"
    }
    /**
     * Return transport type
     * @param {string} e - WebRTC ICE candidate
     * @returns {bool} is support Host
     */
    isHost(e) {
        return e.type === "host"
    }
    /**
     * Return transport type
     * @param {array} tracks - WebRTC media track
     * @returns {string} label of track
     */
    getDeviceName_(tracks) {
        if (tracks.length === 0) {
            return null;
        }
        return tracks[0].label;
    }
    /**
     * Return local media stream. May need to grant permission
     * @param {Object} constraints - getUser media constrain
     * @param {onSuccess} onSuccess - The callback that handles getUserMedia success, and return local stream as a callback parameter
     * @param {onFail} onFail - The callback that handles getUserMedia failuer
     */
    doGetUserMedia(constraints, onSuccess, onFail) {
        var self = this;
        try {
            // Call into getUserMedia via the polyfill (adapter.js).
            navigator.mediaDevices.getUserMedia(constraints)
                .then(function (stream) {
                    var cam = self.getDeviceName_(stream.getVideoTracks());
                    var mic = self.getDeviceName_(stream.getAudioTracks());
                    onSuccess.apply(this, arguments);
                })
                .catch(function (error) {
                    if (onFail) {
                        onFail.apply(this, arguments);
                    } else {
                        console.error(error)
                    }
                });
        } catch (e) {
            console.error(error)
        }
    }
    /**
     * Empty function
     */
    noop() {
    }
}

export {Utils};