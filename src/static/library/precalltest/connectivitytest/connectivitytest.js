'use strict';
import {Utils} from "../lib/utils";
import {WebRTCCon} from "../connectivitytest/webrtconn"

/**
 * @class Verify connections.
 * Establish a loopback connection using
 *  Relay,
 *  Reflexive,
 *  Host only
 *  And check whether connection can be established, and data can be exchange between peer.
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class Connectivity {
    /** @constructs
     * @param {Object} configs - Test Duration. Number of time in second the test will run
     */
    constructor(configs) {
        let self = this;
        self.configs = _.clone(configs, true);
        self.util = new Utils();
        self.testDuration = parseFloat(configs['test-timeout']);
        self.timeout_id = undefined;
    }

    /**
     * Check connectivity detail
     * @param {function} progress - binder to show progress in UI
     * @param {function} log - binder to show logs in UI
     * @returns {Promise} Promise object that contains Relay, Reflexive, Host only connection support details.
     */
    checkConnectivity(progress, log) {
        progress = progress || function () {
        };
        log = log || function () {
        };
        let self = this;
        progress(0);
        let data = {
            'Relay': {'support': false, 'data': undefined},
            'Reflexive': {'support': false, 'data': undefined},
            'Host': {'support': false, 'data': undefined},
        };

        return new Promise((resolve, reject) => {
            function check(process, isGood) {
                return new Promise((resolve, reject) => {
                    process.testConnectivity(isGood).then(function (success) {
                        resolve(success)
                    }).catch(function (err) {
                        resolve(false);
                    })
                });
            }

            log("Info : Checking relayed connectivity...");
            progress(5);
            const relay = new WebRTCCon(self.configs);
            check(relay, self.util.isRelay).then(function (success) {
                progress(20);
                data.Relay.support = success;
                const reflex = new WebRTCCon(self.configs);
                log("Info : Checking reflexive connectivity...");
                check(reflex, self.util.isReflexive).then(function (success) {
                    progress(40);
                    const host = new WebRTCCon(self.configs);
                    data.Reflexive.support = success;
                    log("Info : Checking host connectivity...");
                    check(new WebRTCCon(self.configs), self.util.isHost).then(function (success) {
                        progress(80);
                        data.Host.support = success;
                        resolve(data);
                    });
                });
            });
        });
    }

}

export {Connectivity};
