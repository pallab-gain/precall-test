'use strict';

/**
 * @class Provides available audio, video hardware details. Also provides list of supported video resolutions.
 *
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class Hardware {
    /** @constructs */
    constructor() {
        let self = this;
        self.resolutions = [
            {res: [160, 120], support: false,},
            {res: [320, 180], support: false,},
            {res: [640, 360], support: false,},
            {res: [768, 576], support: false,},
            {res: [1024, 576], support: false,},
            {res: [1280, 720], support: false,},
            {res: [1280, 768], support: false,},
            {res: [1280, 800], support: false,},
            {res: [1920, 1080], support: false,},
            {res: [1920, 1200], support: false,},
            {res: [3840, 2160], support: false,},
            {res: [4096, 2160], support: false,},
        ];
    }
    /**
     * Return available video input devices.
     * @returns {Object} List of available audio, and video devices.
     */
    _deviceList() {
        return navigator.mediaDevices.enumerateDevices();
    }
    /**
     * @returns {Object} List of available audio input devices.
     */
    _audioDeviceList() {
        let self = this;
        return new Promise((resolve, reject) => {
            self._deviceList().then(function (deviceList) {
                const dlist = _.filter(deviceList, {'kind': 'audioinput'});
                resolve(dlist);
            }).catch(function (err) {
                reject(err);
            })
        });
    }
    /**
     * Return available video input devices.
     * @returns {Object} List of available video input devices.
     */
    _videoDeviceList() {
        let self = this;
        return new Promise((resolve, reject) => {
            self._deviceList().then(function (deviceList) {
                const dlist = _.filter(deviceList, {'kind': 'videoinput'});
                resolve(dlist);
            }).catch(function (err) {
                reject(err);
            })
        });
    }
    /**
     * Returns list of available audio input devices
     * @returns {Promise} Promise object represents list of available audio input devices
     */
    audioSupport() {
        let self = this;
        const promise = self._audioDeviceList();
        return new Promise((resolve, reject) => {
            let media = {
                deviceSupport: false,
                deviceList: []
            };
            promise.then(function (success) {
                media.deviceList = success;
                media.deviceSupport = _.size(media.deviceList) > 0 ? true : false;
                if (media.deviceSupport == false) {
                    resolve(media);
                } else {
                    resolve(media);
                }
            }).catch(function (err) {
                reject(media);
            })
        });

    }
    /**
     * Returns list of available video input devices
     * @returns {Promise} Promise object represents list of available video input devices
     */
    videoSupport() {
        let self = this;
        const promise = self._videoDeviceList();
        return new Promise((resolve, reject) => {
            let media = {
                deviceSupport: false,
                deviceList: []
            };
            promise.then(function (success) {
                media.deviceList = success;
                media.deviceSupport = _.size(media.deviceList) > 0 ? true : false;
                if (media.deviceSupport == false) {
                    resolve(media);
                } else {
                    resolve(media);
                }
            }).catch(function (err) {
                reject(media);
            })
        });
    }

    /**
     * Returns list of supported video resolution.
     * @param {function} progress - binder to show progress in UI
     * @returns {Promise} Promise object represents list of supported video resolution.
     */
    resolutionTest(progress) {
        progress = progress || function(){};
        let self = this;
        const totalLength = self.resolutions.length;
        let totalTested = 0;
        progress(0);
        return new Promise((resolve, reject) => {
            for (let indx in self.resolutions) {
                (function (curIndx) {
                    const constraints = {
                        audio: false,
                        video: {
                            width: {exact: self.resolutions[curIndx].res[0]},
                            height: {exact: self.resolutions[curIndx].res[1]}
                        }
                    };
                    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
                        self.resolutions[curIndx].support = true;
                        stream.getTracks().forEach(function (track) {
                            track.stop();
                        });
                        totalTested += 1;
                        progress( Math.round( 100.*totalLength/totalLength ) );
                        if (totalTested >= totalLength) {
                            resolve(self.resolutions);
                        }
                    }).catch(function (err) {
                        self.resolutions[curIndx].support = false;
                        totalTested += 1;
                        progress( Math.round( 100.*totalLength/totalLength ) );
                        if (totalTested >= totalLength) {
                            resolve(self.resolutions);
                        }
                    })
                })(indx)
            }
        });
    }
    /**
     * Returns list of supported devices
     * @param {bool} isAudio - Device type is audio or video
     * @returns {Promise} Promise object represents list of supported video resolution.
     */
    supportDetails(isAudio) {
        let self = this;
        let data = {
            'data': undefined, 'err': undefined
        };
        return new Promise((resolve, reject) => {
            let promise = isAudio ? self.audioSupport() : self.videoSupport();

            promise.then(function (success) {
                data.data = success;
                resolve(data)
            }).catch(function (err) {
                data.err = err;
                resolve(data)
            })
        });
    }
    /**
     * Returns list of supported video resolution
     * @param {function} progress binder to show progress in UI
     * @returns {Promise} Promise object represents list of supported video resolution.
     */
    resolutionSupport(progress) {
        let self = this;
        return new Promise((resolve, reject) => {
           self.resolutionTest(progress).then(function(success){
               resolve(success)
           }).catch(function(err){
               resolve(err);
           });
        });
    }
}

export {Hardware};