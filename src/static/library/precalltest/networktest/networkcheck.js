'use strict';
import {Utils} from "../lib/utils";

/**
 * @class Verify connectivity support list.
 * Make a peer connection using relay server, and verify if connection supports
 *  TCP,
 *  TLS,
 *  UDP
 *  IPv6
 * @author Pallab Gain <pallab.gain.gmail.com>
 * @version 0.1.0
 */

class Network {
    /** @constructs
     * @param {Object} configs - Test Duration, Turn details
     */
    constructor(configs) {
        let self = this;
        self.config = {'iceServers': []};
        self.util = new Utils();
        self.testDuration = parseFloat( configs['test-timeout'] );
        self.turnDetails = {
          urls:  configs['turn-url'],
          username:  configs['turn-uname'],
          credential:  configs['turn-password'],
        };
        self.timeout_id = undefined;
        self.progress = undefined;
        self.log = undefined;
    }
    /**
     * @param {bool} isUDP - Transport type is UDP, or TCP
     * @returns {Object} Turn details for provided transport type
     */
    getRemoteTurn(isUDP) {
        let self = this;
        let iceServer = _.clone(self.turnDetails,true);
        if (isUDP) {
            iceServer.urls = (iceServer.urls+"?transport=udp")
        } else {
            iceServer.urls = (iceServer.urls+"?transport=tcp")
        }
        self.config.iceServers.push(iceServer);
        return self.config;
    }
    /**
     * Collect webRTC ICE candidates
     * @param {function} progress - binder to show progress in UI
     * @param {Object} iceConfigs - Turn ICE configuration
     * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
     */
    collectCandidates(progress,iceConfigs) {
        progress = progress || function () {
        };
        let self = this;
        let config = iceConfigs;
        progress(0);
        let data = {
            'TCP': {'support': false, 'data': undefined},
            'TLS': {'support': false, 'data': undefined},
            'UDP': {'support': false, 'data': undefined},
            'IPv6': {'support': false, 'data': undefined}
        };

        return new Promise((resolve, reject) => {
            if(typeof self.timeout_id!='undefined'){
                clearTimeout(self.timeout_id);
                self.timeout_id = undefined;
            }
            self.timeout_id=setTimeout( function(){
                progress(100);
                if(typeof pc !='undefined') {
                    pc.close();
                    pc = null;
                }
                resolve(data)
            }, self.testDuration*1000);

            let pc = undefined;
            try {
                pc = new RTCPeerConnection(config, null);
            } catch (error) {
                console.error(error);
                reject(error)
            }
            pc.addEventListener('icecandidate', function (e) {
                // Once we've decided, ignore future callbacks.
                if (e.currentTarget.signalingState === 'closed') {
                    return;
                }

                if (e.candidate) {
                    progress(80);
                    var parsed = self.util.parseCandidate(e.candidate.candidate);
                    if (self.util.isTCP(parsed)) {
                        data.TCP = {'support': true, 'data': parsed};
                    } else if (self.util.isTLS(parsed)) {
                        data.TLS = {'support': true, 'data': parsed};
                    } else if (self.util.isUDP(parsed)) {
                        data.UDP = {'support': true, 'data': parsed};
                    } else if (self.util.isIPv6(parsed)) {
                        data.IPv6 = {'support': true, 'data': parsed};
                    }
                } else {
                    if(typeof self.timeout_id!='undefined'){
                        clearTimeout(self.timeout_id);
                        self.timeout_id = undefined;
                    }

                    progress(100);
                    pc.close();
                    pc = null;
                    resolve(data)

                }
            });
            const offerParam = {offerToReceiveAudio: 1};
            pc.createOffer(
                offerParam
            ).then(
                function (offer) {
                    progress(30);
                    pc.setLocalDescription(offer).then(
                        self.util.noop,
                        self.util.noop
                    );
                },
                self.util.noop
            );
        });
    }
    /**
     * Get network connectivity details
     * @param {function} progress - binder to show progress in UI
     * @param {function} log binder to log in UI
     * @returns {Promise} Promise object that contains TCP, TLS, UDP, IPv6 connectivity support details.
     */
    getNetworkDetail(progress,log) {
        progress = progress || function(){};
        let self = this;
        self.progress = progress;
        self.log = log;
        let candidates = self.getRemoteTurn(true);
        candidates = self.getRemoteTurn(false);
        return self.collectCandidates(progress,candidates)
    }


}

export {Network};